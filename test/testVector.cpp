//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include <NonRelocatable/Vector.h>
#include <Relocatable/BoostOffsetPtr/Vector.h>
#include <Relocatable/OffsetPtr/Vector.h>
#include <Relocatable/PtrRelativizer/Vector.h>
#include <array>
#include <gtest/gtest.h>
#include <stdexcept>

class Sample
{
};

template<typename T>
class VectorSampleFixture : public testing::Test
{
};

using VectorSampleTypes = testing::Types<
	NonRelocatable::Vector<Sample>,
	Relocatable::PtrRelativizer::Vector<Sample>,
	Relocatable::OffsetPtr::Vector<Sample>,
	Relocatable::BoostOffsetPtr::Vector<Sample>>;
TYPED_TEST_SUITE(VectorSampleFixture, VectorSampleTypes);

TYPED_TEST(VectorSampleFixture, Instantiate)
{
	TypeParam v{};
	EXPECT_EQ(v.size(), 0);
	EXPECT_EQ(v.capacity(), 0);
}

TYPED_TEST(VectorSampleFixture, InstantiateBounded)
{
	std::array<Sample, 1> buffer;
	TypeParam v{typename TypeParam::BufferView{buffer.data(), buffer.size() * sizeof(Sample)}};
	EXPECT_EQ(v.size(), 0);
	EXPECT_EQ(v.capacity(), 1);
}

template<typename T>
class VectorIntFixture : public testing::Test
{
};

using VectorIntTypes = testing::Types<
	NonRelocatable::Vector<int>,
	Relocatable::PtrRelativizer::Vector<int>,
	Relocatable::OffsetPtr::Vector<int>,
	Relocatable::BoostOffsetPtr::Vector<int>>;
TYPED_TEST_SUITE(VectorIntFixture, VectorIntTypes);

TYPED_TEST(VectorIntFixture, Initialize)
{
	TypeParam v{{17, 19, 23}};
	EXPECT_EQ(v.size(), 3);
	EXPECT_EQ(v[0], 17);
	EXPECT_EQ(v[1], 19);
	EXPECT_EQ(v[2], 23);
	EXPECT_EQ(v.capacity(), 3);
}

TYPED_TEST(VectorIntFixture, InitializeBounded)
{
	std::array<int, 3> buffer;
	TypeParam v{{17, 19, 23}, typename TypeParam::BufferView{buffer.data(), buffer.size() * sizeof(int)}};
	EXPECT_EQ(v.size(), 3);
	EXPECT_EQ(v[0], 17);
	EXPECT_EQ(v[1], 19);
	EXPECT_EQ(v[2], 23);
	EXPECT_EQ(v.capacity(), 3);
}

TYPED_TEST(VectorIntFixture, InitializeBoundedOutOfBounds)
{
	std::array<int, 1> buffer;
	EXPECT_THROW(
		(TypeParam{{17, 19, 23}, typename TypeParam::BufferView{buffer.data(), buffer.size() * sizeof(int)}}), std::bad_alloc);
}

TYPED_TEST(VectorIntFixture, Copy)
{
	TypeParam u{{17, 19, 23}};
	TypeParam v{u};
	EXPECT_EQ(v.size(), 3);
	EXPECT_EQ(v[0], 17);
	EXPECT_EQ(v[1], 19);
	EXPECT_EQ(v[2], 23);
	EXPECT_EQ(v.capacity(), 3);
}

TYPED_TEST(VectorIntFixture, CopyBounded)
{
	std::array<int, 3> buffer;
	TypeParam u{{17, 19, 23}, typename TypeParam::BufferView{buffer.data(), buffer.size() * sizeof(int)}};
	TypeParam v{u};
	EXPECT_EQ(v.size(), 3);
	EXPECT_EQ(v[0], 17);
	EXPECT_EQ(v[1], 19);
	EXPECT_EQ(v[2], 23);
	EXPECT_EQ(v.capacity(), 3);
}

TYPED_TEST(VectorIntFixture, CopyToBounded)
{
	std::array<int, 3> buffer;
	TypeParam u{{17, 19, 23}};
	TypeParam v{u, typename TypeParam::BufferView{buffer.data(), buffer.size() * sizeof(int)}};
	EXPECT_EQ(v.size(), 3);
	EXPECT_EQ(v[0], 17);
	EXPECT_EQ(v[1], 19);
	EXPECT_EQ(v[2], 23);
	EXPECT_EQ(v.capacity(), 3);
}

TYPED_TEST(VectorIntFixture, CopyToBoundedOutOfBounds)
{
	std::array<int, 1> buffer;
	TypeParam u{{17, 19, 23}};
	EXPECT_THROW((TypeParam{u, typename TypeParam::BufferView{buffer.data(), buffer.size() * sizeof(int)}}), std::bad_alloc);
}

TYPED_TEST(VectorIntFixture, ReserveEmpty)
{
	TypeParam v{};
	v.reserve(10);
	EXPECT_EQ(v.size(), 0);
	EXPECT_EQ(v.capacity(), 10);
}

TYPED_TEST(VectorIntFixture, ReserveEmptyBounded)
{
	std::array<int, 11> buffer;
	TypeParam v{typename TypeParam::BufferView{buffer.data(), buffer.size() * sizeof(int)}};
	v.reserve(10);
	EXPECT_EQ(v.size(), 0);
	EXPECT_EQ(v.capacity(), 11);
}

TYPED_TEST(VectorIntFixture, ReserveEmptyOutOfBounds)
{
	std::array<int, 1> buffer;
	TypeParam v{typename TypeParam::BufferView{buffer.data(), buffer.size() * sizeof(int)}};
	EXPECT_THROW(v.reserve(10), std::bad_alloc);
}

TYPED_TEST(VectorIntFixture, Reserve)
{
	TypeParam v{{17, 19, 23}};
	v.reserve(10);
	EXPECT_EQ(v.size(), 3);
	EXPECT_EQ(v[0], 17);
	EXPECT_EQ(v[1], 19);
	EXPECT_EQ(v[2], 23);
	EXPECT_EQ(v.capacity(), 10);
}

TYPED_TEST(VectorIntFixture, ReserveBounded)
{
	std::array<int, 11> buffer;
	TypeParam v{{17, 19, 23}, typename TypeParam::BufferView{buffer.data(), buffer.size() * sizeof(int)}};
	v.reserve(10);
	EXPECT_EQ(v.size(), 3);
	EXPECT_EQ(v[0], 17);
	EXPECT_EQ(v[1], 19);
	EXPECT_EQ(v[2], 23);
	EXPECT_EQ(v.capacity(), 11);
}

TYPED_TEST(VectorIntFixture, ReserveOutOfBounds)
{
	std::array<int, 3> buffer;
	TypeParam v{{17, 19, 23}, typename TypeParam::BufferView{buffer.data(), buffer.size() * sizeof(int)}};
	EXPECT_THROW(v.reserve(10), std::bad_alloc);
}

TYPED_TEST(VectorIntFixture, PushBack)
{
	TypeParam v{{17, 19, 23}};
	v.push_back(29);
	EXPECT_EQ(v.size(), 4);
	EXPECT_EQ(v[0], 17);
	EXPECT_EQ(v[1], 19);
	EXPECT_EQ(v[2], 23);
	EXPECT_EQ(v[3], 29);
	EXPECT_EQ(v.capacity(), 4);
}

TYPED_TEST(VectorIntFixture, PushBackBounded)
{
	std::array<int, 11> buffer;
	TypeParam v{{17, 19, 23}, typename TypeParam::BufferView{buffer.data(), buffer.size() * sizeof(int)}};
	v.push_back(29);
	EXPECT_EQ(v.size(), 4);
	EXPECT_EQ(v[0], 17);
	EXPECT_EQ(v[1], 19);
	EXPECT_EQ(v[2], 23);
	EXPECT_EQ(v[3], 29);
	EXPECT_EQ(v.capacity(), 11);
}

TYPED_TEST(VectorIntFixture, ReservePushBack)
{
	TypeParam v{{17, 19, 23}};
	v.reserve(10);
	v.push_back(29);
	EXPECT_EQ(v.size(), 4);
	EXPECT_EQ(v[0], 17);
	EXPECT_EQ(v[1], 19);
	EXPECT_EQ(v[2], 23);
	EXPECT_EQ(v[3], 29);
	EXPECT_EQ(v.capacity(), 10);
}

TYPED_TEST(VectorIntFixture, ReservePushBackBounded)
{
	std::array<int, 11> buffer;
	TypeParam v{{17, 19, 23}, typename TypeParam::BufferView{buffer.data(), buffer.size() * sizeof(int)}};
	v.reserve(10);
	v.push_back(29);
	EXPECT_EQ(v.size(), 4);
	EXPECT_EQ(v[0], 17);
	EXPECT_EQ(v[1], 19);
	EXPECT_EQ(v[2], 23);
	EXPECT_EQ(v[3], 29);
	EXPECT_EQ(v.capacity(), 11);
}

TYPED_TEST(VectorIntFixture, ReservePushBackOutOfBounds)
{
	std::array<int, 4> buffer;
	TypeParam v{{17, 19, 23}, typename TypeParam::BufferView{buffer.data(), buffer.size() * sizeof(int)}};
	v.reserve(4);
	EXPECT_NO_THROW(v.push_back(29));
	EXPECT_THROW(v.push_back(31), std::bad_alloc);
}

TYPED_TEST(VectorIntFixture, Clear)
{
	TypeParam v{{17, 19, 23}};
	v.clear();
	EXPECT_EQ(v.size(), 0);
	EXPECT_EQ(v.capacity(), 3);
}

TYPED_TEST(VectorIntFixture, ClearBounded)
{
	std::array<int, 3> buffer;
	TypeParam v{{17, 19, 23}, typename TypeParam::BufferView{buffer.data(), buffer.size() * sizeof(int)}};
	v.clear();
	EXPECT_EQ(v.size(), 0);
	EXPECT_EQ(v.capacity(), 3);
}

TYPED_TEST(VectorIntFixture, Assign)
{
	TypeParam v{};
	v.assign({17, 19, 23});
	EXPECT_EQ(v.size(), 3);
	EXPECT_EQ(v[0], 17);
	EXPECT_EQ(v[1], 19);
	EXPECT_EQ(v[2], 23);
	EXPECT_EQ(v.capacity(), 3);
}

TYPED_TEST(VectorIntFixture, AssignBounded)
{
	std::array<int, 3> buffer;
	TypeParam v{typename TypeParam::BufferView{buffer.data(), buffer.size() * sizeof(int)}};
	v.assign({17, 19, 23});
	EXPECT_EQ(v.size(), 3);
	EXPECT_EQ(v[0], 17);
	EXPECT_EQ(v[1], 19);
	EXPECT_EQ(v[2], 23);
	EXPECT_EQ(v.capacity(), 3);
}

TYPED_TEST(VectorIntFixture, AssignBoundedOutOfBounds)
{
	std::array<int, 1> buffer;
	TypeParam v{typename TypeParam::BufferView{buffer.data(), buffer.size() * sizeof(int)}};
	EXPECT_THROW(v.assign({17, 19, 23}), std::bad_alloc);
}

TYPED_TEST(VectorIntFixture, Subscript)
{
	TypeParam v{{17}};
	EXPECT_EQ(v.size(), 1);
	v[0] = 19;
	EXPECT_EQ(v[0], 19);
	EXPECT_EQ(v.capacity(), 1);
}

TYPED_TEST(VectorIntFixture, SubscriptBounded)
{
	std::array<int, 1> buffer;
	TypeParam v{{17}, typename TypeParam::BufferView{buffer.data(), buffer.size() * sizeof(int)}};
	EXPECT_EQ(v.size(), 1);
	v[0] = 19;
	EXPECT_EQ(v[0], 19);
	EXPECT_EQ(v.capacity(), 1);
}

TYPED_TEST(VectorIntFixture, Data)
{
	TypeParam v{{17}};
	EXPECT_EQ(v.size(), 1);
	EXPECT_EQ(*v.data(), 17);
	*v.data() = 19;
	EXPECT_EQ(v[0], 19);
	EXPECT_EQ(v.capacity(), 1);
}

TYPED_TEST(VectorIntFixture, DataBounded)
{
	std::array<int, 1> buffer;
	TypeParam v{{17}, typename TypeParam::BufferView{buffer.data(), buffer.size() * sizeof(int)}};
	EXPECT_EQ(v.size(), 1);
	EXPECT_EQ(*v.data(), 17);
	*v.data() = 19;
	EXPECT_EQ(v[0], 19);
	EXPECT_EQ(v.capacity(), 1);
}

TYPED_TEST(VectorIntFixture, Sort)
{
	TypeParam v{{17, 29, 23, 19}};
	std::sort(v.begin(), v.end());
	EXPECT_EQ(v.size(), 4);
	EXPECT_EQ(v[0], 17);
	EXPECT_EQ(v[1], 19);
	EXPECT_EQ(v[2], 23);
	EXPECT_EQ(v[3], 29);
	EXPECT_EQ(v.capacity(), 4);
}

TYPED_TEST(VectorIntFixture, SortBounded)
{
	std::array<int, 4> buffer;
	TypeParam v{{17, 29, 23, 19}, typename TypeParam::BufferView{buffer.data(), buffer.size() * sizeof(int)}};
	std::sort(v.begin(), v.end());
	EXPECT_EQ(v.size(), 4);
	EXPECT_EQ(v[0], 17);
	EXPECT_EQ(v[1], 19);
	EXPECT_EQ(v[2], 23);
	EXPECT_EQ(v[3], 29);
	EXPECT_EQ(v.capacity(), 4);
}
