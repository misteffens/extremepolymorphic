//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include <PtrRelativizer.h>
#include <cstring>
#include <gtest/gtest.h>

class Sample
{
};

class RelativePtrFixture : public testing::Test, public PtrRelativizer
{
};

class NullableRelativePtrFixture : public testing::Test, public PtrRelativizer
{
};

template<template<typename> typename T>
struct Other : public PtrRelativizer
{
	Other() : ptr(*this, &sample)
	{
	}

	Sample sample;
	T<Sample> ptr;
};

Other<PtrRelativizer::RelativePtr> relativePtrOther;
Other<PtrRelativizer::NullableRelativePtr> nullableRelativePtrOther;

TEST_F(RelativePtrFixture, Nullptr)
{
	RelativePtr<Sample> ptr(*this, nullptr);
	EXPECT_NE(PtrRelativizer{*this}.toPtr(ptr), nullptr);
}

TEST_F(RelativePtrFixture, SamplePtr)
{
	Sample sample;
	RelativePtr<Sample> ptr(*this, &sample);
	EXPECT_EQ(toPtr(ptr), &sample);
}

TEST_F(RelativePtrFixture, CopySamplePtr)
{
	RelativePtr<Sample> ptr(*this, relativePtrOther.toPtr(relativePtrOther.ptr));
	EXPECT_EQ(toPtr(ptr), &relativePtrOther.sample);
}

TEST_F(RelativePtrFixture, AssignSamplePtr)
{
	Sample sample;
	RelativePtr<Sample> ptr(*this, &sample);
	EXPECT_EQ(toPtr(ptr), &sample);
	ptr.assign(*this, relativePtrOther.toPtr(relativePtrOther.ptr));
	EXPECT_EQ(toPtr(ptr), &relativePtrOther.sample);
}

TEST_F(NullableRelativePtrFixture, Nullptr)
{
	NullableRelativePtr<Sample> ptr(*this, nullptr);
	EXPECT_EQ(PtrRelativizer{*this}.toPtr(ptr), nullptr);
}

TEST_F(NullableRelativePtrFixture, SamplePtr)
{
	Sample sample;
	NullableRelativePtr<Sample> ptr(*this, &sample);
	EXPECT_EQ(toPtr(ptr), &sample);
}

TEST_F(NullableRelativePtrFixture, CopySamplePtr)
{
	NullableRelativePtr<Sample> ptr(*this, nullableRelativePtrOther.toPtr(nullableRelativePtrOther.ptr));
	EXPECT_EQ(toPtr(ptr), &nullableRelativePtrOther.sample);
}

TEST_F(NullableRelativePtrFixture, AssignSamplePtr)
{
	Sample sample;
	NullableRelativePtr<Sample> ptr(*this, &sample);
	EXPECT_EQ(toPtr(ptr), &sample);
	ptr.assign(*this, nullableRelativePtrOther.toPtr(nullableRelativePtrOther.ptr));
	EXPECT_EQ(toPtr(ptr), &nullableRelativePtrOther.sample);
}
