//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "expandAndSort.h"
#include "print.h"
#include <BoundedVector.h>
#include <cstddef>
#include <errno.h>
#include <fcntl.h>
#include <iostream>
#include <stdexcept>
#include <sys/mman.h>
#include <unistd.h>

using ShmType = BoundedVector<int, 10>;

void child(int requestPipe, int responsePipe)
{
	char shmName[100];
	if (read(requestPipe, shmName, sizeof(shmName)) < 0) {
		throw std::system_error{errno, std::generic_category(), "Child: cannot read shared memory name from request pipe."};
	}
	std::cerr << "Child: received shared memory name \"" << shmName << "\"." << std::endl;

	// Map shared memory
	int shmfd = shm_open(shmName, O_RDWR, 0600);
	if (shmfd < 0) {
		throw std::system_error{errno, std::generic_category(), "Child: cannot open shared memory."};
	}
	ShmType* shmPtr{reinterpret_cast<ShmType*>(mmap(nullptr, sizeof(ShmType), PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0))};
	if (shmPtr == MAP_FAILED) {
		throw std::system_error{errno, std::generic_category(), "Child: cannot mmap shared memory."};
	}
	std::cerr << "Child: shmPtr=" << shmPtr << std::endl;
	print("Child", shmPtr->v);

	expandAndSort("Child", shmPtr->v);

	std::cerr << "Child: closing response pipe." << std::endl;
	close(responsePipe);
}

int main(int argc, char* argv[])
{
	child(STDIN_FILENO, STDOUT_FILENO);
	return 0;
}
