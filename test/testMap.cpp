//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include <NonRelocatable/Map.h>
#include <Relocatable/BoostOffsetPtr/Map.h>
#include <Relocatable/OffsetPtr/Map.h>
#include <array>
#include <gtest/gtest.h>
#include <map>
#include <stdexcept>

class SampleKey
{
public:
	bool operator<(SampleKey const&) const
	{
		return false;
	}
};

class SampleValue
{
};

template<typename T>
class MapSampleFixture : public testing::Test
{
public:
	T m{};
};

using MapSampleTypes = testing::Types<std::map<SampleKey, SampleValue>, NonRelocatable::Map<SampleKey, SampleValue>>;
TYPED_TEST_SUITE(MapSampleFixture, MapSampleTypes);

TYPED_TEST(MapSampleFixture, Instantiate)
{
	EXPECT_EQ(this->m.size(), 0);
}

TYPED_TEST(MapSampleFixture, Insert1)
{
	EXPECT_TRUE(this->m.insert(std::pair<const SampleKey, SampleValue>{SampleKey{}, SampleValue{}}).second);
	EXPECT_EQ(this->m.size(), 1);
}

using MapIntTypes = testing::
	Types<NonRelocatable::Map<int, int>, Relocatable::BoostOffsetPtr::Map<int, int>, Relocatable::OffsetPtr::Map<int, int>>;

template<typename T>
class MapUnorderedIntFixture : public testing::Test
{
public:
	void SetUp() override
	{
		EXPECT_TRUE(m.insert(std::pair<const int, int>{27235, 15586}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{1970, 1959}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{4130, 6967}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{10650, 16554}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{22918, 17369}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{1011, 5713}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{30728, 2618}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{17597, 10435}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{20572, 3523}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{29073, 4144}).second);
		EXPECT_EQ(m.size(), 10);
	}

	T m{};
};

TYPED_TEST_SUITE(MapUnorderedIntFixture, MapIntTypes);

template<typename T>
class MapBoundedUnorderedIntFixture : public testing::Test
{
public:
	void SetUp() override
	{
		EXPECT_TRUE(m.insert(std::pair<const int, int>{27235, 15586}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{1970, 1959}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{4130, 6967}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{10650, 16554}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{22918, 17369}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{1011, 5713}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{30728, 2618}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{17597, 10435}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{20572, 3523}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{29073, 4144}).second);
		EXPECT_EQ(m.size(), 10);
	}

	alignas(T::alignofNode) std::array<std::byte, 10 * T::sizeofNode> buffer;
	T m{typename T::BufferView{buffer.data(), buffer.size()}};
};

TYPED_TEST_SUITE(MapBoundedUnorderedIntFixture, MapIntTypes);

template<typename T>
class MapAscendingIntFixture : public testing::Test
{
public:
	void SetUp() override
	{
		EXPECT_TRUE(m.insert(std::pair<const int, int>{1011, 5713}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{1970, 1959}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{4130, 6967}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{10650, 16554}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{17597, 10435}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{20572, 3523}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{22918, 17369}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{27235, 15586}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{29073, 4144}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{30728, 2618}).second);
		EXPECT_EQ(m.size(), 10);
	}

	T m{};
};

TYPED_TEST_SUITE(MapAscendingIntFixture, MapIntTypes);

template<typename T>
class MapDescendingIntFixture : public testing::Test
{
public:
	void SetUp() override
	{
		EXPECT_TRUE(m.insert(std::pair<const int, int>{30728, 2618}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{29073, 4144}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{27235, 15586}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{22918, 17369}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{20572, 3523}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{17597, 10435}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{10650, 16554}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{4130, 6967}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{1970, 1959}).second);
		EXPECT_TRUE(m.insert(std::pair<const int, int>{1011, 5713}).second);
		EXPECT_EQ(m.size(), 10);
	}

	T m{};
};

TYPED_TEST_SUITE(MapDescendingIntFixture, MapIntTypes);

TYPED_TEST(MapUnorderedIntFixture, Insert1Success)
{
	EXPECT_TRUE(this->m.insert(std::pair<const int, int>{0, 0}).second);
	EXPECT_EQ(this->m.size(), 11);
}

TYPED_TEST(MapBoundedUnorderedIntFixture, Insert1Success)
{
	EXPECT_THROW(this->m.insert(std::pair<const int, int>{0, 0}), std::bad_alloc);
}

TYPED_TEST(MapUnorderedIntFixture, Insert1Failure)
{
	EXPECT_FALSE(this->m.insert(std::pair<const int, int>{1011, 0}).second);
	EXPECT_EQ(this->m.size(), 10);
}

TYPED_TEST(MapBoundedUnorderedIntFixture, Insert1Failure)
{
	EXPECT_FALSE(this->m.insert(std::pair<const int, int>{1011, 0}).second);
	EXPECT_EQ(this->m.size(), 10);
}

TYPED_TEST(MapUnorderedIntFixture, IteratePreInrement)
{
	typename TypeParam::iterator i{this->m.begin()};
	EXPECT_EQ(*++i, (std::pair<const int, int>{1970, 1959}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{4130, 6967}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{10650, 16554}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{17597, 10435}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{20572, 3523}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{22918, 17369}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{27235, 15586}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{29073, 4144}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{30728, 2618}));
	EXPECT_EQ(++i, this->m.end());
}

TYPED_TEST(MapBoundedUnorderedIntFixture, IteratePreInrement)
{
	typename TypeParam::iterator i{this->m.begin()};
	EXPECT_EQ(*++i, (std::pair<const int, int>{1970, 1959}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{4130, 6967}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{10650, 16554}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{17597, 10435}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{20572, 3523}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{22918, 17369}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{27235, 15586}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{29073, 4144}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{30728, 2618}));
	EXPECT_EQ(++i, this->m.end());
}

TYPED_TEST(MapUnorderedIntFixture, IteratePostInrement)
{
	typename TypeParam::iterator i{this->m.begin()};
	EXPECT_EQ(*i++, (std::pair<const int, int>{1011, 5713}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{1970, 1959}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{4130, 6967}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{10650, 16554}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{17597, 10435}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{20572, 3523}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{22918, 17369}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{27235, 15586}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{29073, 4144}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{30728, 2618}));
	EXPECT_EQ(i, this->m.end());
}

TYPED_TEST(MapBoundedUnorderedIntFixture, IteratePostInrement)
{
	typename TypeParam::iterator i{this->m.begin()};
	EXPECT_EQ(*i++, (std::pair<const int, int>{1011, 5713}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{1970, 1959}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{4130, 6967}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{10650, 16554}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{17597, 10435}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{20572, 3523}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{22918, 17369}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{27235, 15586}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{29073, 4144}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{30728, 2618}));
	EXPECT_EQ(i, this->m.end());
}

TYPED_TEST(MapUnorderedIntFixture, Clear)
{
	this->m.clear();
	EXPECT_EQ(this->m.size(), 0);
}

TYPED_TEST(MapBoundedUnorderedIntFixture, Clear)
{
	this->m.clear();
	EXPECT_EQ(this->m.size(), 0);
}

TYPED_TEST(MapAscendingIntFixture, IteratePreInrement)
{
	typename TypeParam::iterator i{this->m.begin()};
	EXPECT_EQ(*++i, (std::pair<const int, int>{1970, 1959}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{4130, 6967}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{10650, 16554}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{17597, 10435}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{20572, 3523}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{22918, 17369}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{27235, 15586}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{29073, 4144}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{30728, 2618}));
	EXPECT_EQ(++i, this->m.end());
}

TYPED_TEST(MapAscendingIntFixture, IteratePostInrement)
{
	typename TypeParam::iterator i{this->m.begin()};
	EXPECT_EQ(*i++, (std::pair<const int, int>{1011, 5713}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{1970, 1959}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{4130, 6967}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{10650, 16554}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{17597, 10435}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{20572, 3523}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{22918, 17369}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{27235, 15586}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{29073, 4144}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{30728, 2618}));
	EXPECT_EQ(i, this->m.end());
}

TYPED_TEST(MapAscendingIntFixture, Clear)
{
	this->m.clear();
	EXPECT_EQ(this->m.size(), 0);
}

TYPED_TEST(MapDescendingIntFixture, IteratePreInrement)
{
	typename TypeParam::iterator i{this->m.begin()};
	EXPECT_EQ(*++i, (std::pair<const int, int>{1970, 1959}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{4130, 6967}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{10650, 16554}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{17597, 10435}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{20572, 3523}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{22918, 17369}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{27235, 15586}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{29073, 4144}));
	EXPECT_EQ(*++i, (std::pair<const int, int>{30728, 2618}));
	EXPECT_EQ(++i, this->m.end());
}

TYPED_TEST(MapDescendingIntFixture, IteratePostInrement)
{
	typename TypeParam::iterator i{this->m.begin()};
	EXPECT_EQ(*i++, (std::pair<const int, int>{1011, 5713}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{1970, 1959}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{4130, 6967}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{10650, 16554}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{17597, 10435}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{20572, 3523}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{22918, 17369}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{27235, 15586}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{29073, 4144}));
	EXPECT_EQ(*i++, (std::pair<const int, int>{30728, 2618}));
	EXPECT_EQ(i, this->m.end());
}

TYPED_TEST(MapDescendingIntFixture, Clear)
{
	this->m.clear();
	EXPECT_EQ(this->m.size(), 0);
}

TYPED_TEST(MapUnorderedIntFixture, SubscriptExisting)
{
	int& value{this->m[1011]};
	EXPECT_EQ(value, 5713);
	value = 4711;
	EXPECT_EQ(this->m[1011], 4711);
	EXPECT_EQ(this->m.size(), 10);
}

TYPED_TEST(MapBoundedUnorderedIntFixture, SubscriptExisting)
{
	int& value{this->m[1011]};
	EXPECT_EQ(value, 5713);
	value = 4711;
	EXPECT_EQ(this->m[1011], 4711);
	EXPECT_EQ(this->m.size(), 10);
}

TYPED_TEST(MapUnorderedIntFixture, SubscriptNonExisting)
{
	int& value{this->m[0]};
	EXPECT_EQ(value, 0);
	value = 4711;
	EXPECT_EQ(this->m[0], 4711);
	EXPECT_EQ(this->m.size(), 11);
}

TYPED_TEST(MapBoundedUnorderedIntFixture, SubscriptNonExisting)
{
	EXPECT_THROW(this->m[0], std::bad_alloc);
}

TYPED_TEST(MapUnorderedIntFixture, AtExisting)
{
	int& value{this->m.at(1011)};
	EXPECT_EQ(value, 5713);
	value = 4711;
	EXPECT_EQ(this->m.at(1011), 4711);
	EXPECT_EQ(this->m.size(), 10);
}

TYPED_TEST(MapBoundedUnorderedIntFixture, AtExisting)
{
	int& value{this->m.at(1011)};
	EXPECT_EQ(value, 5713);
	value = 4711;
	EXPECT_EQ(this->m.at(1011), 4711);
	EXPECT_EQ(this->m.size(), 10);
}

TYPED_TEST(MapUnorderedIntFixture, AtNonExisting)
{
	EXPECT_THROW(this->m.at(0), std::out_of_range);
}

TYPED_TEST(MapBoundedUnorderedIntFixture, AtNonExisting)
{
	EXPECT_THROW(this->m.at(0), std::out_of_range);
}

TYPED_TEST(MapUnorderedIntFixture, AtExistingConst)
{
	TypeParam const& cm{this->m};
	int const& value{cm.at(1011)};
	EXPECT_EQ(value, 5713);
	EXPECT_EQ(this->m.size(), 10);
}

TYPED_TEST(MapBoundedUnorderedIntFixture, AtExistingConst)
{
	TypeParam const& cm{this->m};
	int const& value{cm.at(1011)};
	EXPECT_EQ(value, 5713);
	EXPECT_EQ(this->m.size(), 10);
}

TYPED_TEST(MapUnorderedIntFixture, AtNonExistingConst)
{
	TypeParam const& cm{this->m};
	EXPECT_THROW(cm.at(0), std::out_of_range);
}

TYPED_TEST(MapBoundedUnorderedIntFixture, AtNonExistingConst)
{
	TypeParam const& cm{this->m};
	EXPECT_THROW(cm.at(0), std::out_of_range);
}
