//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "expandAndSort.h"
#include "print.h"
#include <BoundedVector.h>
#include <stdexcept>

void f()
{
	BoundedVector<int, 10> bv{};
	bv.v.assign({19, 17, 23});
	print("Initial", bv.v);
	expandAndSort("expandAndSort", bv.v);
	print("Final", bv.v);
}

int main(int argc, char* argv[])
{
	f();
	return 0;
}
