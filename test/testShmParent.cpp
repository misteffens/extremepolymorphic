//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "print.h"
#include <BoundedVector.h>
#include <cstddef>
#include <errno.h>
#include <fcntl.h>
#include <iostream>
#include <stdexcept>
#include <sys/mman.h>
#include <sys/wait.h>
#include <unistd.h>

using ShmType = BoundedVector<int, 10>;

void parent(int requestPipe, int responsePipe)
{
	constexpr char const shmName[] = "TrivialityInversion";

	// Set up shared memory
	int shmfd = shm_open(shmName, O_RDWR | O_CREAT, 0600);
	if (shmfd < 0) {
		throw std::system_error{errno, std::generic_category(), "Parent: cannot create shared memory."};
	}
	if (ftruncate(shmfd, sizeof(ShmType)) < 0) {
		throw std::system_error{errno, std::generic_category(), "Parent: cannot resize shared memory."};
	}
	ShmType* shmPtr{reinterpret_cast<ShmType*>(mmap(nullptr, sizeof(ShmType), PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0))};
	if (shmPtr == MAP_FAILED) {
		throw std::system_error{errno, std::generic_category(), "Parent: cannot mmap shared memory."};
	}
	std::cerr << "Parent: shmPtr=" << shmPtr << std::endl;

	new (shmPtr) ShmType{};
	shmPtr->v.assign({19, 17, 23});
	print("Parent", shmPtr->v);

	if (write(requestPipe, shmName, sizeof(shmName)) < 0) {
		throw std::system_error{errno, std::generic_category(), "Parent: cannot write shared memory name to child."};
	}

	char dummy;
	if (read(responsePipe, &dummy, 1) < 0) {
		throw std::system_error{errno, std::generic_category(), "Child: cannot read from response pipe."};
	}
	std::cerr << "Parent: received pipe close." << std::endl;
	print("Parent", shmPtr->v);

	if (shm_unlink(shmName) < 0) {
		throw std::system_error{errno, std::generic_category(), "Parent: cannot unlink shared memory."};
	}
}

int main(int argc, char* argv[])
{
	int requestPipe[2];
	int responsePipe[2];
	if (pipe(requestPipe) < 0 || pipe(responsePipe) < 0) {
		throw std::system_error{errno, std::generic_category(), "Cannot create pipe."};
	}
	pid_t childPid{fork()};
	switch (childPid) {
	case -1:
		throw std::system_error{errno, std::generic_category(), "Cannot fork."};
	case 0:
		// Child
		std::cerr << "Child pid=" << getpid() << std::endl;
		close(requestPipe[1]);
		dup2(requestPipe[0], STDIN_FILENO);
		close(responsePipe[0]);
		dup2(responsePipe[1], STDOUT_FILENO);
		execl(argv[1], argv[1], nullptr);
		throw std::system_error{errno, std::generic_category(), "Cannot execl child."};
	default:
		// Parent
		std::cerr << "Parent pid=" << getpid() << " child pid=" << childPid << std::endl;
		close(requestPipe[0]);
		close(responsePipe[1]);
		parent(requestPipe[1], responsePipe[0]);

		int status;
		waitpid(childPid, &status, 0);
		if (WIFEXITED(status)) {
			std::cerr << "Parent: child pid=" << childPid << " terminated with status " << WEXITSTATUS(status) << std::endl;
		} else if (WIFSIGNALED(status)) {
			std::cerr << "Parent: child pid=" << childPid << " terminated by signal " << WTERMSIG(status) << std::endl;
		} else if (WIFSTOPPED(status)) {
			std::cerr << "Parent: child pid=" << childPid << " stopped by signal " << WSTOPSIG(status) << std::endl;
		} else {
			std::cerr << "Parent: child pid=" << childPid << " terminated abnormally" << std::endl;
		}
	}
	return 0;
}
