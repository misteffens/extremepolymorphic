//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include <OffsetPtr.h>
#include <array>
#include <cstddef>
#include <cstring>
#include <gtest/gtest.h>

class Sample
{
};

class OffsetPtrFixture : public testing::Test
{
public:
	Sample sample;
	OffsetPtr<Sample> ptr{&sample};
};

TEST_F(OffsetPtrFixture, SamplePtr)
{
	EXPECT_EQ(static_cast<Sample*>(ptr), &sample);
}

TEST_F(OffsetPtrFixture, CopySamplePtr)
{
	OffsetPtr<Sample> target(ptr);
	EXPECT_EQ(static_cast<Sample*>(target), &sample);
}

TEST_F(OffsetPtrFixture, AssignSamplePtr)
{
	OffsetPtr<Sample> target(nullptr);
	target = ptr;
	EXPECT_EQ(static_cast<Sample*>(target), &sample);
}

TEST(OffsetPtr, Nullptr)
{
	OffsetPtr<Sample> ptr(nullptr);
	union
	{
		std::array<std::byte, sizeof(OffsetPtr<Sample>)> storage_;
		OffsetPtr<Sample> ptr_;
	} target{};
	std::memcpy(&target.ptr_, &ptr, sizeof(OffsetPtr<Sample>));
	EXPECT_NE(static_cast<Sample*>(target.ptr_), nullptr);
}

class NullableOffsetPtrFixture : public testing::Test
{
public:
	Sample sample;
	NullableOffsetPtr<Sample> ptr{&sample};
};
TEST_F(NullableOffsetPtrFixture, SamplePtr)
{
	EXPECT_EQ(static_cast<Sample*>(ptr), &sample);
}

TEST_F(NullableOffsetPtrFixture, CopySamplePtr)
{
	NullableOffsetPtr<Sample> target(ptr);
	EXPECT_EQ(static_cast<Sample*>(target), &sample);
}

TEST_F(NullableOffsetPtrFixture, AssignSamplePtr)
{
	NullableOffsetPtr<Sample> target(nullptr);
	target = ptr;
	EXPECT_EQ(static_cast<Sample*>(target), &sample);
}

TEST(NullableOffsetPtr, Nullptr)
{
	NullableOffsetPtr<Sample> ptr(nullptr);
	union
	{
		std::array<std::byte, sizeof(NullableOffsetPtr<Sample>)> storage_;
		NullableOffsetPtr<Sample> ptr_;
	} target{};
	std::memcpy(&target.ptr_, &ptr, sizeof(NullableOffsetPtr<Sample>));
	EXPECT_EQ(static_cast<Sample*>(target.ptr_), nullptr);
}
