//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include <BoundedVector.h>
#include <array>
#include <gtest/gtest.h>
#include <stdexcept>

template<typename T>
class ConstructionCounter
{
public:
	static ConstructionCounter& getInstance()
	{
		static ConstructionCounter instance;
		return instance;
	}

	void registerInvocation()
	{
		++invocations_;
	}

	unsigned invocations() const
	{
		return invocations_;
	}

	void reset()
	{
		invocations_ = 0;
	}


private:
	ConstructionCounter() = default;

	unsigned invocations_{0};
};

struct Sample
{
	Sample()
	{
		ConstructionCounter<Sample>::getInstance().registerInvocation();
	}
};

TEST(BoundedVector, Instantiate)
{
	ConstructionCounter<Sample>::getInstance().reset();
	BoundedVector<Sample, 3> bv{};
	EXPECT_EQ(ConstructionCounter<Sample>::getInstance().invocations(), 0);
	EXPECT_EQ(bv.v.size(), 0);
	EXPECT_EQ(bv.v.capacity(), 3);
}

TEST(BoundedVector, Copy)
{
	BoundedVector<int, 3> bu{};
	bu.v.assign({17, 19, 23});
	EXPECT_EQ(bu.v.size(), 3);
	EXPECT_EQ(bu.v.capacity(), 3);
	EXPECT_THROW(bu.v.push_back(29), std::bad_alloc);
	BoundedVector<int, 3> bv{bu};
	bv.v.push_back(29);
	EXPECT_EQ(bv.v.size(), 4);
	EXPECT_EQ(bv.v.capacity(), 4);
}
