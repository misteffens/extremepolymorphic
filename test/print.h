//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#ifndef print_h_INCLUDED
#define print_h_INCLUDED

#include <Map.h>
#include <Vector.h>
#include <iostream>

template<typename T>
void print(char const* prefix, Vector<T> const& v)
{
	for (std::size_t i{0}; i < v.size(); ++i) {
		std::cerr << prefix << ": v[" << i << "]=" << v[i] << std::endl;
	}
};

template<typename Key, typename T>
void print(char const* prefix, Map<Key, T>& m)
{
	for (typename Map<Key, T>::iterator i{m.begin()}; i != m.end(); ++i) {
		std::cerr << prefix << ": m[" << i->first << "]=" << i->second << std::endl;
	}
};

#endif // print_h_INCLUDED
