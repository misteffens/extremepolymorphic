//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#ifndef expandAndSort_h_INCLUDED
#define expandAndSort_h_INCLUDED

#include <Vector.h>

void expandAndSort(char const* prefix, Vector<int>& v);

#endif // expandAndSort_h_INCLUDED
