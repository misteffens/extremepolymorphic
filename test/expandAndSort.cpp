//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "expandAndSort.h"
#include "print.h"
#include <algorithm>
#include <iostream>

void expandAndSort(char const* prefix, Vector<int>& v)
{
	std::cerr << prefix << ": appending four elements." << std::endl;
	v.push_back(7);
	v.push_back(5);
	v.push_back(3);
	v.push_back(2);
	print(prefix, v);
	std::cerr << prefix << ": sorting ..." << std::endl;
	std::sort(v.begin(), v.end());
	print(prefix, v);
}
