# TrivialityInversion Test

Testing behavior of a POC container name `Vector` in both relocatable and non-relocatable incarnations. Class under test can be customized in

include/Vector.h

with the alias using either `NonRelocatable::Vector` for absolute pointer use, or

* `Relocatable::PtrRelativizer::Vector` for `RelativePtr`,
* `Relocatable::BoostOffsetPtr::Vector` for `boost::interprocess::offset_ptr`,
* `Relocatable::Offset::Vector` for simplified offset pointer

use.

The APIs being tests for compatibility are a template for printing Vector elements as multiline list, and function `expandAndSort(char const*, Vector<int>&)` adding four numbers in reverse order to its `Vector` argument, then sorting the entire result in place. The latter is provisioned as a static or a shared library `ExpandAndSort` to all test below, and has a completely storage agnostic interface.

## Shared memory

Tests are using a

```
struct BoundedVector
{
	Vector<int> v{Vector<int>::BufferView{buffer.data(), buffer.size()}};
	std::array<int, 10> buffer;
};
```

of integer values with a capacity of ten elements. The memory segment is shared between a parent and child process, synchronizing via pipes connected to the child's STDIN/STDOUT. Inside the build directory execute test by running

```
test/testShmParent test/testShmChild
```

### Failure

Using `NonRelocatable::Vector` the child crashes immediately when attempting to traverse the vector for printing. The parent finds its content unchanged. Example:

```
Parent pid=5305 child pid=5306
Parent: shmPtr=0x7557363e0000
Parent: v[0]=19
Parent: v[1]=17
Parent: v[2]=23
Child pid=5306
Child: received shared memory name "TrivialityInversion".
Child: shmPtr=0x7aed6b921000
Child: v[0]=Parent: received pipe close.
Parent: v[0]=19
Parent: v[1]=17
Parent: v[2]=23
Parent: child pid=5306 terminated by signal 11
```

### Success

Using `Relocatable::PtrRelativizer::Vector` the child terminates normally, runs `expandAndSort` while printing `Vector` contents before, during, and after. The parent finally finds the vector in a consistent state, with the number and order of elements produced by the child. Example:

```
Parent pid=5489 child pid=5490
Parent: shmPtr=0x7ab276c0b000
Parent: v[0]=19
Parent: v[1]=17
Parent: v[2]=23
Child pid=5490
Child: received shared memory name "TrivialityInversion".
Child: shmPtr=0x7d92d5fd1000
Child: v[0]=19
Child: v[1]=17
Child: v[2]=23
Child: appending four elements.
Child: v[0]=19
Child: v[1]=17
Child: v[2]=23
Child: v[3]=7
Child: v[4]=5
Child: v[5]=3
Child: v[6]=2
Child: sorting ...
Child: v[0]=2
Child: v[1]=3
Child: v[2]=5
Child: v[3]=7
Child: v[4]=17
Child: v[5]=19
Child: v[6]=23
Child: closing response pipe.
Parent: received pipe close.
Parent: v[0]=2
Parent: v[1]=3
Parent: v[2]=5
Parent: v[3]=7
Parent: v[4]=17
Parent: v[5]=19
Parent: v[6]=23
Parent: child pid=5490 terminated with status 0
```

## Stack storage

```
test/testStackStorage
```

demonstrates the same operation as before, but accomodating the `BoundedVector` as local variable of function `f()`, thus stored within that function's stack frame.

The test succeeds with both `Relocatable::PtrRelativizer::Vector<T>` and `NonRelocatable::Vector<T>`, as there is no displacement in virtual address space involved.

## Dynamic storage

```
test/testDynamicStorage
```

drops boundaries of `BoundedVector`, instead instantiating a `Vector` directly. No fixed size buffer is used, which results in the default constructed `std::pmr::polymorphic_allocator<int>` allocating storage dynamically from its associated memory resource. The vector is initialized with ten elements, such that the subsequent call of `expandAndSort` will grow content beyond the previous bounds.

The test succeeds with both `Relocatable::PtrRelativizer::Vector<T>` and `NonRelocatable::Vector<T>`, as there is no displacement in virtual address space involved.
