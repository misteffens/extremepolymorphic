//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "insertFour.h"
#include "print.h"
#include <algorithm>
#include <iostream>

void insertFour(char const* prefix, Map<int, int>& m)
{
	std::cerr << prefix << ": inserting four elements." << std::endl;
	m.insert(std::pair<const int, int>{30728, 2618});
	m.insert(std::pair<const int, int>{17597, 10435});
	m.insert(std::pair<const int, int>{20572, 3523});
	m.insert(std::pair<const int, int>{29073, 4144});
	print(prefix, m);
}
