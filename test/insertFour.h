//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#ifndef insertFour_h_INCLUDED
#define insertFour_h_INCLUDED

#include <Map.h>

void insertFour(char const* prefix, Map<int, int>& m);

#endif // insertFour_h_INCLUDED
