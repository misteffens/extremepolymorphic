//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include "Vector.h"
#include "expandAndSort.h"
#include "print.h"
#include <stdexcept>

void f()
{
	Vector<int> v{{47, 19, 17, 23, 29, 37, 31, 41, 43, 53}};
	print("Initial", v);
	expandAndSort("expandAndSort", v);
	print("Final", v);
}

int main(int argc, char* argv[])
{
	f();
	return 0;
}
