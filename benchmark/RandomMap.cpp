//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include <NonRelocatable/Map.h>
#include <Relocatable/BoostOffsetPtr/Map.h>
#include <Relocatable/OffsetPtr/Map.h>
#include <array>
#include <benchmark/benchmark.h>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/containers/map.hpp>
#include <boost/interprocess/managed_heap_memory.hpp>
#include <random>
#include <scoped_allocator>

constexpr std::size_t size{100000};

template<template<typename, typename> typename MapType, typename T>
void BM_RandomMap(benchmark::State& state)
{
	std::mt19937 gen{};
	std::uniform_int_distribution<std::size_t> keyDistr{};
	std::uniform_int_distribution<T> valueDistr{};

	MapType<std::size_t, T> m;
	std::array<std::size_t, size> keys;
	for (auto i{keys.begin()}; i != keys.end(); ++i) {
		*i = keyDistr(gen);
		m.insert(std::pair<std::size_t const, T>{*i, keyDistr(gen)});
	}
	std::array<std::size_t, size> rKeys;
	std::uniform_int_distribution<std::size_t> indexDistr(0, keys.size() - 1);
	for (auto i{rKeys.begin()}; i != rKeys.end(); ++i) {
		*i = keys[indexDistr(gen)];
	}

	// MEASUREMENT
	for (auto _ : state) {
		T result{0};
		for (std::size_t i{0}; i < rKeys.size(); ++i) {
			result ^= m.at(rKeys[i]);
		}
		benchmark::DoNotOptimize(result);
		benchmark::ClobberMemory();
	}
}

template<typename T>
using alloc = boost::interprocess::allocator<T, boost::interprocess::managed_heap_memory::segment_manager>;

template<typename T>
void BM_RandomBoostInterprocessMap(benchmark::State& state)
{
	std::mt19937 gen{};
	std::uniform_int_distribution<std::size_t> keyDistr{};
	std::uniform_int_distribution<T> valueDistr{};

	boost::interprocess::managed_heap_memory heapMemory(size * (100 + sizeof(std::pair<const std::size_t, T>)));
	boost::interprocess::map<std::size_t, T, std::less<std::size_t>, alloc<std::pair<const std::size_t, T>>> m{
		heapMemory.get_segment_manager()};
	std::array<std::size_t, size> keys;
	for (auto i{keys.begin()}; i != keys.end(); ++i) {
		*i = keyDistr(gen);
		m.insert(std::pair<std::size_t const, T>{*i, keyDistr(gen)});
	}
	std::array<std::size_t, size> rKeys;
	std::uniform_int_distribution<std::size_t> indexDistr(0, keys.size() - 1);
	for (auto i{rKeys.begin()}; i != rKeys.end(); ++i) {
		*i = keys[indexDistr(gen)];
	}

	// MEASUREMENT
	for (auto _ : state) {
		T result{0};
		for (std::size_t i{0}; i < rKeys.size(); ++i) {
			result ^= m.at(rKeys[i]);
		}
		benchmark::DoNotOptimize(result);
		benchmark::ClobberMemory();
	}
}

BENCHMARK(BM_RandomMap<std::map, std::uint8_t>);
BENCHMARK(BM_RandomBoostInterprocessMap<std::uint8_t>);
BENCHMARK(BM_RandomMap<NonRelocatable::Map, std::uint8_t>);
BENCHMARK(BM_RandomMap<Relocatable::BoostOffsetPtr::Map, std::uint8_t>);
BENCHMARK(BM_RandomMap<Relocatable::OffsetPtr::Map, std::uint8_t>);
BENCHMARK(BM_RandomMap<std::map, std::uint32_t>);
BENCHMARK(BM_RandomBoostInterprocessMap<std::uint32_t>);
BENCHMARK(BM_RandomMap<NonRelocatable::Map, std::uint32_t>);
BENCHMARK(BM_RandomMap<Relocatable::BoostOffsetPtr::Map, std::uint32_t>);
BENCHMARK(BM_RandomMap<Relocatable::OffsetPtr::Map, std::uint32_t>);
BENCHMARK(BM_RandomMap<std::map, std::uint64_t>);
BENCHMARK(BM_RandomBoostInterprocessMap<std::uint64_t>);
BENCHMARK(BM_RandomMap<NonRelocatable::Map, std::uint64_t>);
BENCHMARK(BM_RandomMap<Relocatable::BoostOffsetPtr::Map, std::uint64_t>);
BENCHMARK(BM_RandomMap<Relocatable::OffsetPtr::Map, std::uint64_t>);
