//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include <NonRelocatable/Vector.h>
#include <Relocatable/BoostOffsetPtr/Vector.h>
#include <Relocatable/OffsetPtr/Vector.h>
#include <Relocatable/PtrRelativizer/Vector.h>
#include <array>
#include <benchmark/benchmark.h>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/managed_heap_memory.hpp>
#include <random>
#include <scoped_allocator>

constexpr std::size_t size{1000};

template<template<typename> typename VectorType, typename T>
void BM_Iterate2D(benchmark::State& state)
{
	std::mt19937 gen{};
	std::uniform_int_distribution<std::size_t> distr(0, size - 1);

	std::array<std::size_t, size * size> rIndex;
	for (auto i{rIndex.begin()}; i != rIndex.end(); ++i) {
		*i = distr(gen);
	}
	VectorType<VectorType<T>> v;
	v.reserve(size);
	for (std::size_t i{0}; i < size; ++i) {
		v.push_back(VectorType<T>{});
		v[i].assign(rIndex.data() + i * size, rIndex.data() + (i + 1) * size);
	}
	// MEASUREMENT
	for (auto _ : state) {
		T result{0};
		for (auto i{v.begin()}; i != v.end(); ++i) {
			for (auto j{i->begin()}; j != i->end(); ++j) {
				result ^= *j;
			}
		}
		benchmark::DoNotOptimize(result);
		benchmark::ClobberMemory();
	}
}

template<typename T>
using alloc = boost::interprocess::allocator<T, boost::interprocess::managed_heap_memory::segment_manager>;

template<typename T>
void BM_Iterate2DBoostInterprocessVector(benchmark::State& state)
{
	std::mt19937 gen{};
	std::uniform_int_distribution<std::size_t> distr(0, size - 1);

	std::array<std::size_t, size * size> rIndex;
	for (auto i{rIndex.begin()}; i != rIndex.end(); ++i) {
		*i = distr(gen);
	}
	boost::interprocess::managed_heap_memory heapMemory(size * (1000 + sizeof(T) * size));
	boost::interprocess::vector<
		boost::interprocess::vector<T, alloc<T>>,
		std::scoped_allocator_adaptor<alloc<boost::interprocess::vector<T, alloc<T>>>>>
		v{heapMemory.get_segment_manager()};
	v.resize(size);
	for (std::size_t i{0}; i < size; ++i) {
		v[i].assign(rIndex.data() + i * size, rIndex.data() + (i + 1) * size);
	}
	// MEASUREMENT
	for (auto _ : state) {
		T result{0};
		for (auto i{v.begin()}; i != v.end(); ++i) {
			for (auto j{i->begin()}; j != i->end(); ++j) {
				result ^= *j;
			}
		}
		benchmark::DoNotOptimize(result);
		benchmark::ClobberMemory();
	}
}

BENCHMARK(BM_Iterate2D<std::vector, std::uint8_t>);
BENCHMARK(BM_Iterate2DBoostInterprocessVector<std::uint8_t>);
BENCHMARK(BM_Iterate2D<NonRelocatable::Vector, std::uint8_t>);
BENCHMARK(BM_Iterate2D<Relocatable::PtrRelativizer::Vector, std::uint8_t>);
BENCHMARK(BM_Iterate2D<Relocatable::BoostOffsetPtr::Vector, std::uint8_t>);
BENCHMARK(BM_Iterate2D<Relocatable::OffsetPtr::Vector, std::uint8_t>);
BENCHMARK(BM_Iterate2D<std::vector, std::uint32_t>);
BENCHMARK(BM_Iterate2DBoostInterprocessVector<std::uint32_t>);
BENCHMARK(BM_Iterate2D<NonRelocatable::Vector, std::uint32_t>);
BENCHMARK(BM_Iterate2D<Relocatable::PtrRelativizer::Vector, std::uint32_t>);
BENCHMARK(BM_Iterate2D<Relocatable::BoostOffsetPtr::Vector, std::uint32_t>);
BENCHMARK(BM_Iterate2D<Relocatable::OffsetPtr::Vector, std::uint32_t>);
BENCHMARK(BM_Iterate2D<std::vector, std::uint64_t>);
BENCHMARK(BM_Iterate2DBoostInterprocessVector<std::uint64_t>);
BENCHMARK(BM_Iterate2D<NonRelocatable::Vector, std::uint64_t>);
BENCHMARK(BM_Iterate2D<Relocatable::PtrRelativizer::Vector, std::uint64_t>);
BENCHMARK(BM_Iterate2D<Relocatable::BoostOffsetPtr::Vector, std::uint64_t>);
BENCHMARK(BM_Iterate2D<Relocatable::OffsetPtr::Vector, std::uint64_t>);
