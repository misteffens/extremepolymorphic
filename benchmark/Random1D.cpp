//
// Copyright (C) 2023 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#include <NonRelocatable/Vector.h>
#include <Relocatable/BoostOffsetPtr/Vector.h>
#include <Relocatable/OffsetPtr/Vector.h>
#include <Relocatable/PtrRelativizer/Vector.h>
#include <array>
#include <benchmark/benchmark.h>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/managed_heap_memory.hpp>
#include <random>

constexpr std::size_t size{1000};

template<template<typename> typename VectorType, typename T>
void BM_Random1D(benchmark::State& state)
{
	std::mt19937 gen{};
	std::uniform_int_distribution<std::size_t> distr(0, size - 1);

	std::vector<std::size_t> rIndex(size * size);
	for (auto i{rIndex.begin()}; i != rIndex.end(); ++i) {
		*i = distr(gen);
	}
	VectorType<T> v{rIndex.begin(), rIndex.end()};
	std::array<std::size_t, size * size / 2> flatIndex;
	for (std::size_t i{0}; i < flatIndex.size(); ++i) {
		flatIndex[i] = rIndex[2 * i] * size + rIndex[2 * i + 1];
	}
	// MEASUREMENT
	for (auto _ : state) {
		T result{0};
		for (std::size_t i{0}; i < flatIndex.size(); ++i) {
			result ^= v[flatIndex[i]];
		}
		benchmark::DoNotOptimize(result);
		benchmark::ClobberMemory();
	}
}

template<typename T>
using alloc = boost::interprocess::allocator<T, boost::interprocess::managed_heap_memory::segment_manager>;

template<typename T>
void BM_Random1DBoostInterprocessVector(benchmark::State& state)
{
	std::mt19937 gen{};
	std::uniform_int_distribution<std::size_t> distr(0, size - 1);

	std::vector<std::size_t> rIndex(size * size);
	for (auto i{rIndex.begin()}; i != rIndex.end(); ++i) {
		*i = distr(gen);
	}
	boost::interprocess::managed_heap_memory heapMemory(size * (1000 + sizeof(T) * size));
	boost::interprocess::vector<T, alloc<T>> v{heapMemory.get_segment_manager()};
	v.assign(rIndex.begin(), rIndex.end());
	std::array<std::size_t, size * size / 2> flatIndex;
	for (std::size_t i{0}; i < flatIndex.size(); ++i) {
		flatIndex[i] = rIndex[2 * i] * size + rIndex[2 * i + 1];
	}
	// MEASUREMENT
	for (auto _ : state) {
		T result{0};
		for (std::size_t i{0}; i < flatIndex.size(); ++i) {
			result ^= v[flatIndex[i]];
		}
		benchmark::DoNotOptimize(result);
		benchmark::ClobberMemory();
	}
}

BENCHMARK(BM_Random1D<std::vector, std::uint8_t>);
BENCHMARK(BM_Random1DBoostInterprocessVector<std::uint8_t>);
BENCHMARK(BM_Random1D<NonRelocatable::Vector, std::uint8_t>);
BENCHMARK(BM_Random1D<Relocatable::PtrRelativizer::Vector, std::uint8_t>);
BENCHMARK(BM_Random1D<Relocatable::BoostOffsetPtr::Vector, std::uint8_t>);
BENCHMARK(BM_Random1D<Relocatable::OffsetPtr::Vector, std::uint8_t>);
BENCHMARK(BM_Random1D<std::vector, std::uint32_t>);
BENCHMARK(BM_Random1DBoostInterprocessVector<std::uint32_t>);
BENCHMARK(BM_Random1D<NonRelocatable::Vector, std::uint32_t>);
BENCHMARK(BM_Random1D<Relocatable::PtrRelativizer::Vector, std::uint32_t>);
BENCHMARK(BM_Random1D<Relocatable::BoostOffsetPtr::Vector, std::uint32_t>);
BENCHMARK(BM_Random1D<Relocatable::OffsetPtr::Vector, std::uint32_t>);
BENCHMARK(BM_Random1D<std::vector, std::uint64_t>);
BENCHMARK(BM_Random1DBoostInterprocessVector<std::uint64_t>);
BENCHMARK(BM_Random1D<NonRelocatable::Vector, std::uint64_t>);
BENCHMARK(BM_Random1D<Relocatable::PtrRelativizer::Vector, std::uint64_t>);
BENCHMARK(BM_Random1D<Relocatable::BoostOffsetPtr::Vector, std::uint64_t>);
BENCHMARK(BM_Random1D<Relocatable::OffsetPtr::Vector, std::uint64_t>);
