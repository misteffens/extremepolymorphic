# Benchmarks

## Random2D: Targetting the worst case

The benchmark is designed to be most sensitive for storage pointer dereference impact. A 1000 by 1000 square of unsigned random numbers is set up as nested vectors:

![Random2D pattern](doc/Random2D.svg)

A total of 500000 read access operations to random (better: chaotic) row and column indices are used to compute a cumulative XOR-result, which is finally protected against elimination by compiler optimization. The two dimensional array consists of 1000 inner column objects, each with its individual storage pointer. Randomized access across this amount of inner vectors aims to prevent a smart compiler from caching results of pointer offset calculations in CPU registers. Instead we aim to enforce pristine storage pointer dereferencing with every access operation.

For reproducibility, the sequence of one million random numbers needed for both array initialization and indexing are precomputed using the mt19937 generator, uniform integer distribution and a fixed seed value.

For comparison, `std::vector` is included. All measurements are performed with vector element sizes of 8, 32, and 64 bits.

In more balanced use cases, e.g. fewer subscript operations per iteration, or iterators being used for traversal, the impact of relative vs. absolute addressing can be expected significantly smaller.

### Intel XEON W3550

```
2024-05-12T09:12:19+02:00
Running benchmark/benchmarkRandom2D
Run on (4 X 2492.69 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x4)
  L1 Instruction 32 KiB (x4)
  L2 Unified 256 KiB (x4)
  L3 Unified 8192 KiB (x1)
Load Average: 0.08, 0.06, 0.01
----------------------------------------------------------------------------------------------------------
Benchmark                                                                Time             CPU   Iterations
----------------------------------------------------------------------------------------------------------
BM_Random2D<std::vector, std::uint8_t>                             1772038 ns      1771836 ns          392
BM_Random2DBoostInterprocessVector<std::uint8_t>                   2170212 ns      2169972 ns          323
BM_Random2D<NonRelocatable::Vector, std::uint8_t>                  1870938 ns      1870562 ns          376
BM_Random2D<Relocatable::PtrRelativizer::Vector, std::uint8_t>     1883099 ns      1882938 ns          369
BM_Random2D<Relocatable::BoostOffsetPtr::Vector, std::uint8_t>     2219282 ns      2218753 ns          315
BM_Random2D<Relocatable::OffsetPtr::Vector, std::uint8_t>          1889072 ns      1888720 ns          370
BM_Random2D<std::vector, std::uint32_t>                            2957420 ns      2956433 ns          237
BM_Random2DBoostInterprocessVector<std::uint32_t>                  3485733 ns      3485390 ns          200
BM_Random2D<NonRelocatable::Vector, std::uint32_t>                 3008603 ns      3007924 ns          232
BM_Random2D<Relocatable::PtrRelativizer::Vector, std::uint32_t>    3036574 ns      3036194 ns          231
BM_Random2D<Relocatable::BoostOffsetPtr::Vector, std::uint32_t>    3507495 ns      3506775 ns          200
BM_Random2D<Relocatable::OffsetPtr::Vector, std::uint32_t>         3035387 ns      3034982 ns          231
BM_Random2D<std::vector, std::uint64_t>                            4253483 ns      4252740 ns          163
BM_Random2DBoostInterprocessVector<std::uint64_t>                  5219777 ns      5218636 ns          133
BM_Random2D<NonRelocatable::Vector, std::uint64_t>                 4342724 ns      4342350 ns          161
BM_Random2D<Relocatable::PtrRelativizer::Vector, std::uint64_t>    4358140 ns      4356837 ns          161
BM_Random2D<Relocatable::BoostOffsetPtr::Vector, std::uint64_t>    5243572 ns      5242408 ns          133
BM_Random2D<Relocatable::OffsetPtr::Vector, std::uint64_t>         4355019 ns      4353628 ns          161
```

### ARMv8, Broadcom BCM2711, Quad core Cortex-A72 (Raspberry Pi 4 Model B)

```
2024-05-12T09:47:13+02:00
Running benchmark/benchmarkRandom2D
Run on (4 X 1800 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x4)
  L1 Instruction 48 KiB (x4)
  L2 Unified 1024 KiB (x1)
Load Average: 0.36, 0.11, 0.03
----------------------------------------------------------------------------------------------------------
Benchmark                                                                Time             CPU   Iterations
----------------------------------------------------------------------------------------------------------
BM_Random2D<std::vector, std::uint8_t>                             8798806 ns      8797505 ns           79
BM_Random2DBoostInterprocessVector<std::uint8_t>                  11462514 ns     11460669 ns           61
BM_Random2D<NonRelocatable::Vector, std::uint8_t>                  9447259 ns      9446825 ns           74
BM_Random2D<Relocatable::PtrRelativizer::Vector, std::uint8_t>    10157889 ns     10157890 ns           69
BM_Random2D<Relocatable::BoostOffsetPtr::Vector, std::uint8_t>    12404822 ns     12404822 ns           56
BM_Random2D<Relocatable::OffsetPtr::Vector, std::uint8_t>         10822032 ns     10822028 ns           64
BM_Random2D<std::vector, std::uint32_t>                           13377597 ns     13377365 ns           52
BM_Random2DBoostInterprocessVector<std::uint32_t>                 14359542 ns     14359000 ns           49
BM_Random2D<NonRelocatable::Vector, std::uint32_t>                13638678 ns     13638233 ns           51
BM_Random2D<Relocatable::PtrRelativizer::Vector, std::uint32_t>   13803290 ns     13803289 ns           51
BM_Random2D<Relocatable::BoostOffsetPtr::Vector, std::uint32_t>   15116108 ns     15114584 ns           46
BM_Random2D<Relocatable::OffsetPtr::Vector, std::uint32_t>        13996428 ns     13996437 ns           50
BM_Random2D<std::vector, std::uint64_t>                           14412945 ns     14412951 ns           48
BM_Random2DBoostInterprocessVector<std::uint64_t>                 16184157 ns     16184214 ns           43
BM_Random2D<NonRelocatable::Vector, std::uint64_t>                14805537 ns     14805152 ns           47
BM_Random2D<Relocatable::PtrRelativizer::Vector, std::uint64_t>   15053602 ns     15053617 ns           46
BM_Random2D<Relocatable::BoostOffsetPtr::Vector, std::uint64_t>   16750845 ns     16750854 ns           42
BM_Random2D<Relocatable::OffsetPtr::Vector, std::uint64_t>        15450644 ns     15450688 ns           45
```

### AMD Ryzen 7 5700U

```
2024-05-13T07:55:45+02:00
Running ./benchmarkRandom2D
Run on (16 X 1775.45 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x8)
  L1 Instruction 32 KiB (x8)
  L2 Unified 512 KiB (x8)
  L3 Unified 4096 KiB (x2)
Load Average: 0.66, 0.26, 0.12
----------------------------------------------------------------------------------------------------------
Benchmark                                                                Time             CPU   Iterations
----------------------------------------------------------------------------------------------------------
BM_Random2D<std::vector, std::uint8_t>                              822652 ns       822566 ns          850
BM_Random2DBoostInterprocessVector<std::uint8_t>                   1143047 ns      1142995 ns          613
BM_Random2D<NonRelocatable::Vector, std::uint8_t>                   861354 ns       861339 ns          814
BM_Random2D<Relocatable::PtrRelativizer::Vector, std::uint8_t>      940389 ns       940333 ns          745
BM_Random2D<Relocatable::BoostOffsetPtr::Vector, std::uint8_t>     1177813 ns      1177814 ns          593
BM_Random2D<Relocatable::OffsetPtr::Vector, std::uint8_t>           943779 ns       943634 ns          742
BM_Random2D<std::vector, std::uint32_t>                            2728424 ns      2728377 ns          257
BM_Random2DBoostInterprocessVector<std::uint32_t>                  4410314 ns      4409797 ns          159
BM_Random2D<NonRelocatable::Vector, std::uint32_t>                 2775261 ns      2775133 ns          253
BM_Random2D<Relocatable::PtrRelativizer::Vector, std::uint32_t>    3291973 ns      3291588 ns          213
BM_Random2D<Relocatable::BoostOffsetPtr::Vector, std::uint32_t>    4451972 ns      4451901 ns          157
BM_Random2D<Relocatable::OffsetPtr::Vector, std::uint32_t>         3291473 ns      3291367 ns          213
BM_Random2D<std::vector, std::uint64_t>                            3370570 ns      3370252 ns          207
BM_Random2DBoostInterprocessVector<std::uint64_t>                  5391485 ns      5391111 ns          129
BM_Random2D<NonRelocatable::Vector, std::uint64_t>                 3409757 ns      3409640 ns          205
BM_Random2D<Relocatable::PtrRelativizer::Vector, std::uint64_t>    4012385 ns      4012095 ns          175
BM_Random2D<Relocatable::BoostOffsetPtr::Vector, std::uint64_t>    5454228 ns      5453824 ns          128
BM_Random2D<Relocatable::OffsetPtr::Vector, std::uint64_t>         4003488 ns      4003316 ns          174
```

### AMD Ryzen 7 7840U

```
2024-05-12T10:27:11+02:00
Running benchmark/benchmarkRandom2D
Run on (16 X 400 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x8)
  L1 Instruction 32 KiB (x8)
  L2 Unified 1024 KiB (x8)
  L3 Unified 16384 KiB (x1)
Load Average: 0.10, 0.28, 0.17
----------------------------------------------------------------------------------------------------------
Benchmark                                                                Time             CPU   Iterations
----------------------------------------------------------------------------------------------------------
BM_Random2D<std::vector, std::uint8_t>                              393636 ns       393617 ns         1777
BM_Random2DBoostInterprocessVector<std::uint8_t>                    583306 ns       583278 ns         1194
BM_Random2D<NonRelocatable::Vector, std::uint8_t>                   413943 ns       413922 ns         1691
BM_Random2D<Relocatable::PtrRelativizer::Vector, std::uint8_t>      456968 ns       456942 ns         1534
BM_Random2D<Relocatable::BoostOffsetPtr::Vector, std::uint8_t>      594313 ns       594277 ns         1180
BM_Random2D<Relocatable::OffsetPtr::Vector, std::uint8_t>           469779 ns       469753 ns         1496
BM_Random2D<std::vector, std::uint32_t>                             469741 ns       469710 ns         1550
BM_Random2DBoostInterprocessVector<std::uint32_t>                   697676 ns       697631 ns          996
BM_Random2D<NonRelocatable::Vector, std::uint32_t>                  579931 ns       579905 ns         1000
BM_Random2D<Relocatable::PtrRelativizer::Vector, std::uint32_t>     632217 ns       632176 ns         1184
BM_Random2D<Relocatable::BoostOffsetPtr::Vector, std::uint32_t>     777397 ns       777323 ns          919
BM_Random2D<Relocatable::OffsetPtr::Vector, std::uint32_t>          696171 ns       696118 ns         1036
BM_Random2D<std::vector, std::uint64_t>                            1266613 ns      1266501 ns          472
BM_Random2DBoostInterprocessVector<std::uint64_t>                  1234792 ns      1234736 ns          529
BM_Random2D<NonRelocatable::Vector, std::uint64_t>                  739000 ns       738945 ns          932
BM_Random2D<Relocatable::PtrRelativizer::Vector, std::uint64_t>    1164783 ns      1164633 ns          657
BM_Random2D<Relocatable::BoostOffsetPtr::Vector, std::uint64_t>    1387626 ns      1387512 ns          428
BM_Random2D<Relocatable::OffsetPtr::Vector, std::uint64_t>         1502871 ns      1502416 ns          371
```

## Random1D

This benchmark is a variation of Random2D and processing the same data, but organized in a flat single dimensional vector. Only one storage pointer, either relative or absolute, is involved.

### Intel XEON W3550

```
2024-05-12T09:14:08+02:00
Running benchmark/benchmarkRandom1D
Run on (4 X 3066.61 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x4)
  L1 Instruction 32 KiB (x4)
  L2 Unified 256 KiB (x4)
  L3 Unified 8192 KiB (x1)
Load Average: 0.06, 0.07, 0.02
----------------------------------------------------------------------------------------------------------
Benchmark                                                                Time             CPU   Iterations
----------------------------------------------------------------------------------------------------------
BM_Random1D<std::vector, std::uint8_t>                             1006518 ns      1006428 ns          688
BM_Random1DBoostInterprocessVector<std::uint8_t>                   1005050 ns      1004905 ns          693
BM_Random1D<NonRelocatable::Vector, std::uint8_t>                  1005463 ns      1005384 ns          691
BM_Random1D<Relocatable::PtrRelativizer::Vector, std::uint8_t>     1005649 ns      1005559 ns          690
BM_Random1D<Relocatable::BoostOffsetPtr::Vector, std::uint8_t>     1005479 ns      1005337 ns          693
BM_Random1D<Relocatable::OffsetPtr::Vector, std::uint8_t>          1007396 ns      1007347 ns          693
BM_Random1D<std::vector, std::uint32_t>                            2037953 ns      2036227 ns          350
BM_Random1DBoostInterprocessVector<std::uint32_t>                  2091170 ns      2091082 ns          327
BM_Random1D<NonRelocatable::Vector, std::uint32_t>                 1999780 ns      1999394 ns          350
BM_Random1D<Relocatable::PtrRelativizer::Vector, std::uint32_t>    1991752 ns      1991618 ns          350
BM_Random1D<Relocatable::BoostOffsetPtr::Vector, std::uint32_t>    1998025 ns      1997702 ns          351
BM_Random1D<Relocatable::OffsetPtr::Vector, std::uint32_t>         1991923 ns      1991734 ns          350
BM_Random1D<std::vector, std::uint64_t>                            3092566 ns      3092101 ns          226
BM_Random1DBoostInterprocessVector<std::uint64_t>                  3112709 ns      3112384 ns          225
BM_Random1D<NonRelocatable::Vector, std::uint64_t>                 3109514 ns      3109216 ns          225
BM_Random1D<Relocatable::PtrRelativizer::Vector, std::uint64_t>    3109573 ns      3109265 ns          225
BM_Random1D<Relocatable::BoostOffsetPtr::Vector, std::uint64_t>    3112155 ns      3111818 ns          225
BM_Random1D<Relocatable::OffsetPtr::Vector, std::uint64_t>         3109977 ns      3109391 ns          225
```

### ARMv8, Broadcom BCM2711, Quad core Cortex-A72 (Raspberry Pi 4 Model B)

```
2024-05-12T09:48:20+02:00
Running benchmark/benchmarkRandom1D
Run on (4 X 1800 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x4)
  L1 Instruction 48 KiB (x4)
  L2 Unified 1024 KiB (x1)
Load Average: 0.29, 0.15, 0.05
----------------------------------------------------------------------------------------------------------
Benchmark                                                                Time             CPU   Iterations
----------------------------------------------------------------------------------------------------------
BM_Random1D<std::vector, std::uint8_t>                             5623852 ns      5623853 ns          125
BM_Random1DBoostInterprocessVector<std::uint8_t>                   5641345 ns      5641062 ns          124
BM_Random1D<NonRelocatable::Vector, std::uint8_t>                  5639469 ns      5639024 ns          124
BM_Random1D<Relocatable::PtrRelativizer::Vector, std::uint8_t>     5631370 ns      5631000 ns          124
BM_Random1D<Relocatable::BoostOffsetPtr::Vector, std::uint8_t>     5663717 ns      5663340 ns          123
BM_Random1D<Relocatable::OffsetPtr::Vector, std::uint8_t>          5662077 ns      5661443 ns          123
BM_Random1D<std::vector, std::uint32_t>                           12046687 ns     12046197 ns           58
BM_Random1DBoostInterprocessVector<std::uint32_t>                 12055068 ns     12054569 ns           58
BM_Random1D<NonRelocatable::Vector, std::uint32_t>                12038412 ns     12037449 ns           58
BM_Random1D<Relocatable::PtrRelativizer::Vector, std::uint32_t>   12031553 ns     12024256 ns           58
BM_Random1D<Relocatable::BoostOffsetPtr::Vector, std::uint32_t>   12031170 ns     12029661 ns           58
BM_Random1D<Relocatable::OffsetPtr::Vector, std::uint32_t>        12019236 ns     12018740 ns           58
BM_Random1D<std::vector, std::uint64_t>                           12973070 ns     12972557 ns           54
BM_Random1DBoostInterprocessVector<std::uint64_t>                 12971744 ns     12970265 ns           54
BM_Random1D<NonRelocatable::Vector, std::uint64_t>                12974483 ns     12974507 ns           54
BM_Random1D<Relocatable::PtrRelativizer::Vector, std::uint64_t>   12976656 ns     12976199 ns           54
BM_Random1D<Relocatable::BoostOffsetPtr::Vector, std::uint64_t>   12974958 ns     12974546 ns           54
BM_Random1D<Relocatable::OffsetPtr::Vector, std::uint64_t>        12979603 ns     12978648 ns           54
```

### AMD Ryzen 7 5700U

```
2024-05-13T07:55:26+02:00
Running ./benchmarkRandom1D
Run on (16 X 400 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x8)
  L1 Instruction 32 KiB (x8)
  L2 Unified 512 KiB (x8)
  L3 Unified 4096 KiB (x2)
Load Average: 0.52, 0.20, 0.10
----------------------------------------------------------------------------------------------------------
Benchmark                                                                Time             CPU   Iterations
----------------------------------------------------------------------------------------------------------
BM_Random1D<std::vector, std::uint8_t>                              493847 ns       493819 ns         1431
BM_Random1DBoostInterprocessVector<std::uint8_t>                    486109 ns       486065 ns         1440
BM_Random1D<NonRelocatable::Vector, std::uint8_t>                   489252 ns       489201 ns         1432
BM_Random1D<Relocatable::PtrRelativizer::Vector, std::uint8_t>      486370 ns       486353 ns         1438
BM_Random1D<Relocatable::BoostOffsetPtr::Vector, std::uint8_t>      487071 ns       487013 ns         1343
BM_Random1D<Relocatable::OffsetPtr::Vector, std::uint8_t>           486436 ns       486422 ns         1438
BM_Random1D<std::vector, std::uint32_t>                            1637648 ns      1637461 ns          427
BM_Random1DBoostInterprocessVector<std::uint32_t>                  1636165 ns      1636086 ns          427
BM_Random1D<NonRelocatable::Vector, std::uint32_t>                 1637695 ns      1637616 ns          415
BM_Random1D<Relocatable::PtrRelativizer::Vector, std::uint32_t>    1637726 ns      1637570 ns          427
BM_Random1D<Relocatable::BoostOffsetPtr::Vector, std::uint32_t>    1636744 ns      1636645 ns          427
BM_Random1D<Relocatable::OffsetPtr::Vector, std::uint32_t>         1637882 ns      1637883 ns          426
BM_Random1D<std::vector, std::uint64_t>                            2143922 ns      2143788 ns          326
BM_Random1DBoostInterprocessVector<std::uint64_t>                  2144899 ns      2144812 ns          326
BM_Random1D<NonRelocatable::Vector, std::uint64_t>                 2145152 ns      2144950 ns          326
BM_Random1D<Relocatable::PtrRelativizer::Vector, std::uint64_t>    2145066 ns      2145071 ns          326
BM_Random1D<Relocatable::BoostOffsetPtr::Vector, std::uint64_t>    2145745 ns      2145513 ns          326
BM_Random1D<Relocatable::OffsetPtr::Vector, std::uint64_t>         2145400 ns      2145335 ns          326
```

### AMD Ryzen 7 7840U

```
2024-05-12T10:27:59+02:00
Running benchmark/benchmarkRandom1D
Run on (16 X 3056.71 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x8)
  L1 Instruction 32 KiB (x8)
  L2 Unified 1024 KiB (x8)
  L3 Unified 16384 KiB (x1)
Load Average: 0.44, 0.36, 0.20
----------------------------------------------------------------------------------------------------------
Benchmark                                                                Time             CPU   Iterations
----------------------------------------------------------------------------------------------------------
BM_Random1D<std::vector, std::uint8_t>                              233538 ns       233518 ns         3001
BM_Random1DBoostInterprocessVector<std::uint8_t>                    235688 ns       235677 ns         2987
BM_Random1D<NonRelocatable::Vector, std::uint8_t>                   233537 ns       233511 ns         2996
BM_Random1D<Relocatable::PtrRelativizer::Vector, std::uint8_t>      233137 ns       233120 ns         2998
BM_Random1D<Relocatable::BoostOffsetPtr::Vector, std::uint8_t>      232834 ns       232811 ns         3000
BM_Random1D<Relocatable::OffsetPtr::Vector, std::uint8_t>           233358 ns       233345 ns         2997
BM_Random1D<std::vector, std::uint32_t>                             302097 ns       302071 ns         2318
BM_Random1DBoostInterprocessVector<std::uint32_t>                   302797 ns       302779 ns         2308
BM_Random1D<NonRelocatable::Vector, std::uint32_t>                  302196 ns       302173 ns         2316
BM_Random1D<Relocatable::PtrRelativizer::Vector, std::uint32_t>     299626 ns       299610 ns         2333
BM_Random1D<Relocatable::BoostOffsetPtr::Vector, std::uint32_t>     302368 ns       302347 ns         2312
BM_Random1D<Relocatable::OffsetPtr::Vector, std::uint32_t>          300790 ns       300768 ns         2334
BM_Random1D<std::vector, std::uint64_t>                             403963 ns       403944 ns         1612
BM_Random1DBoostInterprocessVector<std::uint64_t>                   394395 ns       394364 ns         1777
BM_Random1D<NonRelocatable::Vector, std::uint64_t>                  372765 ns       372740 ns         1870
BM_Random1D<Relocatable::PtrRelativizer::Vector, std::uint64_t>     373935 ns       373905 ns         1885
BM_Random1D<Relocatable::BoostOffsetPtr::Vector, std::uint64_t>     374651 ns       374625 ns         1866
BM_Random1D<Relocatable::OffsetPtr::Vector, std::uint64_t>          374873 ns       374844 ns         1863
```


## Sequential2D

### Intel XEON W3550

```
2024-05-12T09:41:35+02:00
Running benchmark/benchmarkSequential2D
Run on (4 X 3066.61 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x4)
  L1 Instruction 32 KiB (x4)
  L2 Unified 256 KiB (x4)
  L3 Unified 8192 KiB (x1)
Load Average: 0.27, 0.11, 0.03
--------------------------------------------------------------------------------------------------------------
Benchmark                                                                    Time             CPU   Iterations
--------------------------------------------------------------------------------------------------------------
BM_Sequential2D<std::vector, std::uint8_t>                               55626 ns        55622 ns        12506
BM_Sequential2DBoostInterprocessVector<std::uint8_t>                     56013 ns        56010 ns        12486
BM_Sequential2D<NonRelocatable::Vector, std::uint8_t>                    63877 ns        63870 ns        11063
BM_Sequential2D<Relocatable::PtrRelativizer::Vector, std::uint8_t>       63850 ns        63848 ns        10886
BM_Sequential2D<Relocatable::BoostOffsetPtr::Vector, std::uint8_t>       66116 ns        66115 ns        10667
BM_Sequential2D<Relocatable::OffsetPtr::Vector, std::uint8_t>            64717 ns        64712 ns        10787
BM_Sequential2D<std::vector, std::uint32_t>                             183050 ns       183039 ns         3823
BM_Sequential2DBoostInterprocessVector<std::uint32_t>                   183436 ns       183423 ns         3813
BM_Sequential2D<NonRelocatable::Vector, std::uint32_t>                  183968 ns       183959 ns         3804
BM_Sequential2D<Relocatable::PtrRelativizer::Vector, std::uint32_t>     187719 ns       187702 ns         3731
BM_Sequential2D<Relocatable::BoostOffsetPtr::Vector, std::uint32_t>     188015 ns       188004 ns         3728
BM_Sequential2D<Relocatable::OffsetPtr::Vector, std::uint32_t>          187750 ns       187736 ns         3728
BM_Sequential2D<std::vector, std::uint64_t>                             495189 ns       495173 ns         1412
BM_Sequential2DBoostInterprocessVector<std::uint64_t>                   492768 ns       492730 ns         1419
BM_Sequential2D<NonRelocatable::Vector, std::uint64_t>                  495930 ns       495860 ns         1299
BM_Sequential2D<Relocatable::PtrRelativizer::Vector, std::uint64_t>     479983 ns       479942 ns         1457
BM_Sequential2D<Relocatable::BoostOffsetPtr::Vector, std::uint64_t>     496646 ns       496610 ns         1416
BM_Sequential2D<Relocatable::OffsetPtr::Vector, std::uint64_t>          498437 ns       498391 ns         1388
```

### ARMv8, Broadcom BCM2711, Quad core Cortex-A72 (Raspberry Pi 4 Model B)

```
2024-05-12T09:49:11+02:00
Running benchmark/benchmarkSequential2D
Run on (4 X 1800 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x4)
  L1 Instruction 48 KiB (x4)
  L2 Unified 1024 KiB (x1)
Load Average: 0.25, 0.17, 0.06
--------------------------------------------------------------------------------------------------------------
Benchmark                                                                    Time             CPU   Iterations
--------------------------------------------------------------------------------------------------------------
BM_Sequential2D<std::vector, std::uint8_t>                              153849 ns       153831 ns         3991
BM_Sequential2DBoostInterprocessVector<std::uint8_t>                    156033 ns       156012 ns         4464
BM_Sequential2D<NonRelocatable::Vector, std::uint8_t>                   234529 ns       234515 ns         3137
BM_Sequential2D<Relocatable::PtrRelativizer::Vector, std::uint8_t>      243693 ns       243647 ns         2858
BM_Sequential2D<Relocatable::BoostOffsetPtr::Vector, std::uint8_t>      261834 ns       261834 ns         2733
BM_Sequential2D<Relocatable::OffsetPtr::Vector, std::uint8_t>           265832 ns       265811 ns         2626
BM_Sequential2D<std::vector, std::uint32_t>                             812822 ns       812821 ns          850
BM_Sequential2DBoostInterprocessVector<std::uint32_t>                   823165 ns       822957 ns          846
BM_Sequential2D<NonRelocatable::Vector, std::uint32_t>                  830581 ns       830408 ns          838
BM_Sequential2D<Relocatable::PtrRelativizer::Vector, std::uint32_t>     830420 ns       830316 ns          836
BM_Sequential2D<Relocatable::BoostOffsetPtr::Vector, std::uint32_t>     831305 ns       831252 ns          841
BM_Sequential2D<Relocatable::OffsetPtr::Vector, std::uint32_t>          831576 ns       831415 ns          839
BM_Sequential2D<std::vector, std::uint64_t>                            1880113 ns      1879898 ns          373
BM_Sequential2DBoostInterprocessVector<std::uint64_t>                  1788313 ns      1788250 ns          390
BM_Sequential2D<NonRelocatable::Vector, std::uint64_t>                 1911200 ns      1911141 ns          366
BM_Sequential2D<Relocatable::PtrRelativizer::Vector, std::uint64_t>    1912965 ns      1912253 ns          366
BM_Sequential2D<Relocatable::BoostOffsetPtr::Vector, std::uint64_t>    1911896 ns      1911529 ns          366
BM_Sequential2D<Relocatable::OffsetPtr::Vector, std::uint64_t>         1908452 ns      1908446 ns          366
```

### AMD Ryzen 7 5700U

```
2024-05-13T07:56:19+02:00
Running ./benchmarkSequential2D
Run on (16 X 1775.86 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x8)
  L1 Instruction 32 KiB (x8)
  L2 Unified 512 KiB (x8)
  L3 Unified 4096 KiB (x2)
Load Average: 0.79, 0.33, 0.15
--------------------------------------------------------------------------------------------------------------
Benchmark                                                                    Time             CPU   Iterations
--------------------------------------------------------------------------------------------------------------
BM_Sequential2D<std::vector, std::uint8_t>                               20312 ns        20311 ns        34548
BM_Sequential2DBoostInterprocessVector<std::uint8_t>                     20534 ns        20533 ns        34114
BM_Sequential2D<NonRelocatable::Vector, std::uint8_t>                    22922 ns        22921 ns        30297
BM_Sequential2D<Relocatable::PtrRelativizer::Vector, std::uint8_t>       33200 ns        33200 ns        20990
BM_Sequential2D<Relocatable::BoostOffsetPtr::Vector, std::uint8_t>       33626 ns        33624 ns        20747
BM_Sequential2D<Relocatable::OffsetPtr::Vector, std::uint8_t>            33549 ns        33547 ns        20854
BM_Sequential2D<std::vector, std::uint32_t>                             146117 ns       146098 ns         4787
BM_Sequential2DBoostInterprocessVector<std::uint32_t>                   146635 ns       146635 ns         4761
BM_Sequential2D<NonRelocatable::Vector, std::uint32_t>                  173842 ns       173833 ns         4037
BM_Sequential2D<Relocatable::PtrRelativizer::Vector, std::uint32_t>     146477 ns       146469 ns         4768
BM_Sequential2D<Relocatable::BoostOffsetPtr::Vector, std::uint32_t>     175293 ns       175285 ns         4000
BM_Sequential2D<Relocatable::OffsetPtr::Vector, std::uint32_t>          173985 ns       173979 ns         3999
BM_Sequential2D<std::vector, std::uint64_t>                             387508 ns       387438 ns         1806
BM_Sequential2DBoostInterprocessVector<std::uint64_t>                   406358 ns       406338 ns         1724
BM_Sequential2D<NonRelocatable::Vector, std::uint64_t>                  388661 ns       388649 ns         1804
BM_Sequential2D<Relocatable::PtrRelativizer::Vector, std::uint64_t>     390124 ns       390108 ns         1805
BM_Sequential2D<Relocatable::BoostOffsetPtr::Vector, std::uint64_t>     390501 ns       390467 ns         1791
BM_Sequential2D<Relocatable::OffsetPtr::Vector, std::uint64_t>          389981 ns       389958 ns         1794
```

### AMD Ryzen 7 7840U

```
2024-05-12T10:29:03+02:00
Running benchmark/benchmarkSequential2D
Run on (16 X 3074.8 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x8)
  L1 Instruction 32 KiB (x8)
  L2 Unified 1024 KiB (x8)
  L3 Unified 16384 KiB (x1)
Load Average: 0.40, 0.38, 0.22
--------------------------------------------------------------------------------------------------------------
Benchmark                                                                    Time             CPU   Iterations
--------------------------------------------------------------------------------------------------------------
BM_Sequential2D<std::vector, std::uint8_t>                               14771 ns        14770 ns        47379
BM_Sequential2DBoostInterprocessVector<std::uint8_t>                     15016 ns        15015 ns        46730
BM_Sequential2D<NonRelocatable::Vector, std::uint8_t>                    16944 ns        16943 ns        40107
BM_Sequential2D<Relocatable::PtrRelativizer::Vector, std::uint8_t>       18080 ns        18078 ns        38050
BM_Sequential2D<Relocatable::BoostOffsetPtr::Vector, std::uint8_t>       27442 ns        27441 ns        25846
BM_Sequential2D<Relocatable::OffsetPtr::Vector, std::uint8_t>            18417 ns        18416 ns        37354
BM_Sequential2D<std::vector, std::uint32_t>                              58388 ns        58385 ns        11989
BM_Sequential2DBoostInterprocessVector<std::uint32_t>                    58781 ns        58777 ns        11900
BM_Sequential2D<NonRelocatable::Vector, std::uint32_t>                   78153 ns        78147 ns         8949
BM_Sequential2D<Relocatable::PtrRelativizer::Vector, std::uint32_t>      78342 ns        78336 ns         8927
BM_Sequential2D<Relocatable::BoostOffsetPtr::Vector, std::uint32_t>      59249 ns        59245 ns        11797
BM_Sequential2D<Relocatable::OffsetPtr::Vector, std::uint32_t>           59327 ns        59323 ns        11781
BM_Sequential2D<std::vector, std::uint64_t>                             108361 ns       108351 ns         6462
BM_Sequential2DBoostInterprocessVector<std::uint64_t>                   108063 ns       108055 ns         6478
BM_Sequential2D<NonRelocatable::Vector, std::uint64_t>                  129476 ns       129466 ns         5403
BM_Sequential2D<Relocatable::PtrRelativizer::Vector, std::uint64_t>     109960 ns       109952 ns         6370
BM_Sequential2D<Relocatable::BoostOffsetPtr::Vector, std::uint64_t>     130080 ns       130072 ns         5361
BM_Sequential2D<Relocatable::OffsetPtr::Vector, std::uint64_t>          109939 ns       109931 ns         6360
```


## Iterate2D

### Intel XEON W3550

```
2024-05-12T09:42:57+02:00
Running benchmark/benchmarkIterate2D
Run on (4 X 3066.61 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x4)
  L1 Instruction 32 KiB (x4)
  L2 Unified 256 KiB (x4)
  L3 Unified 8192 KiB (x1)
Load Average: 0.44, 0.20, 0.07
-----------------------------------------------------------------------------------------------------------
Benchmark                                                                 Time             CPU   Iterations
-----------------------------------------------------------------------------------------------------------
BM_Iterate2D<std::vector, std::uint8_t>                               55142 ns        55139 ns        12703
BM_Iterate2DBoostInterprocessVector<std::uint8_t>                   1227442 ns      1227145 ns          570
BM_Iterate2D<NonRelocatable::Vector, std::uint8_t>                    63684 ns        63668 ns        11055
BM_Iterate2D<Relocatable::PtrRelativizer::Vector, std::uint8_t>       62315 ns        62299 ns        11085
BM_Iterate2D<Relocatable::BoostOffsetPtr::Vector, std::uint8_t>       65245 ns        65173 ns        10776
BM_Iterate2D<Relocatable::OffsetPtr::Vector, std::uint8_t>            66533 ns        66526 ns        10514
BM_Iterate2D<std::vector, std::uint32_t>                             185521 ns       185497 ns         3778
BM_Iterate2DBoostInterprocessVector<std::uint32_t>                  1009649 ns      1009599 ns          692
BM_Iterate2D<NonRelocatable::Vector, std::uint32_t>                  184299 ns       184276 ns         3796
BM_Iterate2D<Relocatable::PtrRelativizer::Vector, std::uint32_t>     184210 ns       184196 ns         3791
BM_Iterate2D<Relocatable::BoostOffsetPtr::Vector, std::uint32_t>     183628 ns       183615 ns         3805
BM_Iterate2D<Relocatable::OffsetPtr::Vector, std::uint32_t>          184198 ns       184147 ns         3803
BM_Iterate2D<std::vector, std::uint64_t>                             494610 ns       494487 ns         1412
BM_Iterate2DBoostInterprocessVector<std::uint64_t>                  1317079 ns      1316974 ns          532
BM_Iterate2D<NonRelocatable::Vector, std::uint64_t>                  496423 ns       496258 ns         1409
BM_Iterate2D<Relocatable::PtrRelativizer::Vector, std::uint64_t>     485022 ns       484843 ns         1442
BM_Iterate2D<Relocatable::BoostOffsetPtr::Vector, std::uint64_t>     498065 ns       497848 ns         1393
BM_Iterate2D<Relocatable::OffsetPtr::Vector, std::uint64_t>          500073 ns       499949 ns         1386
```

### ARMv8, Broadcom BCM2711, Quad core Cortex-A72 (Raspberry Pi 4 Model B)

```
2024-05-12T09:50:00+02:00
Running benchmark/benchmarkIterate2D
Run on (4 X 1800 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x4)
  L1 Instruction 48 KiB (x4)
  L2 Unified 1024 KiB (x1)
Load Average: 0.28, 0.20, 0.08
-----------------------------------------------------------------------------------------------------------
Benchmark                                                                 Time             CPU   Iterations
-----------------------------------------------------------------------------------------------------------
BM_Iterate2D<std::vector, std::uint8_t>                              152820 ns       152808 ns         4354
BM_Iterate2DBoostInterprocessVector<std::uint8_t>                   2258050 ns      2257812 ns          310
BM_Iterate2D<NonRelocatable::Vector, std::uint8_t>                   232632 ns       232615 ns         3207
BM_Iterate2D<Relocatable::PtrRelativizer::Vector, std::uint8_t>      271086 ns       271055 ns         2583
BM_Iterate2D<Relocatable::BoostOffsetPtr::Vector, std::uint8_t>      285023 ns       285013 ns         2477
BM_Iterate2D<Relocatable::OffsetPtr::Vector, std::uint8_t>           288987 ns       288986 ns         2432
BM_Iterate2D<std::vector, std::uint32_t>                             819072 ns       819073 ns          845
BM_Iterate2DBoostInterprocessVector<std::uint32_t>                  2191492 ns      2188724 ns          320
BM_Iterate2D<NonRelocatable::Vector, std::uint32_t>                  828213 ns       828003 ns          844
BM_Iterate2D<Relocatable::PtrRelativizer::Vector, std::uint32_t>     827495 ns       827437 ns          839
BM_Iterate2D<Relocatable::BoostOffsetPtr::Vector, std::uint32_t>     833981 ns       832097 ns          841
BM_Iterate2D<Relocatable::OffsetPtr::Vector, std::uint32_t>          840855 ns       837449 ns          831
BM_Iterate2D<std::vector, std::uint64_t>                            1905354 ns      1897883 ns          370
BM_Iterate2DBoostInterprocessVector<std::uint64_t>                  2365808 ns      2357965 ns          297
BM_Iterate2D<NonRelocatable::Vector, std::uint64_t>                 1917824 ns      1916260 ns          364
BM_Iterate2D<Relocatable::PtrRelativizer::Vector, std::uint64_t>    1903790 ns      1903602 ns          367
BM_Iterate2D<Relocatable::BoostOffsetPtr::Vector, std::uint64_t>    1789542 ns      1789308 ns          387
BM_Iterate2D<Relocatable::OffsetPtr::Vector, std::uint64_t>         1916255 ns      1916113 ns          365
```

### AMD Ryzen 7 5700U

```
2024-05-13T10:25:21+02:00
Running ./benchmarkIterate2D
Run on (16 X 1776.52 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x8)
  L1 Instruction 32 KiB (x8)
  L2 Unified 512 KiB (x8)
  L3 Unified 4096 KiB (x2)
Load Average: 0.13, 0.10, 0.03
-----------------------------------------------------------------------------------------------------------
Benchmark                                                                 Time             CPU   Iterations
-----------------------------------------------------------------------------------------------------------
BM_Iterate2D<std::vector, std::uint8_t>                               20366 ns        20366 ns        34446
BM_Iterate2DBoostInterprocessVector<std::uint8_t>                    733584 ns       733564 ns          954
BM_Iterate2D<NonRelocatable::Vector, std::uint8_t>                    23025 ns        23024 ns        30442
BM_Iterate2D<Relocatable::PtrRelativizer::Vector, std::uint8_t>       24106 ns        24105 ns        28923
BM_Iterate2D<Relocatable::BoostOffsetPtr::Vector, std::uint8_t>       25409 ns        25408 ns        27542
BM_Iterate2D<Relocatable::OffsetPtr::Vector, std::uint8_t>            25149 ns        25148 ns        27873
BM_Iterate2D<std::vector, std::uint32_t>                              68620 ns        68613 ns        10159
BM_Iterate2DBoostInterprocessVector<std::uint32_t>                   641665 ns       641661 ns         1089
BM_Iterate2D<NonRelocatable::Vector, std::uint32_t>                   68945 ns        68944 ns        10083
BM_Iterate2D<Relocatable::PtrRelativizer::Vector, std::uint32_t>     122025 ns       122024 ns         5730
BM_Iterate2D<Relocatable::BoostOffsetPtr::Vector, std::uint32_t>      69836 ns        69835 ns        10017
BM_Iterate2D<Relocatable::OffsetPtr::Vector, std::uint32_t>           69737 ns        69732 ns        10057
BM_Iterate2D<std::vector, std::uint64_t>                             392242 ns       392240 ns         1786
BM_Iterate2DBoostInterprocessVector<std::uint64_t>                   760255 ns       760220 ns          919
BM_Iterate2D<NonRelocatable::Vector, std::uint64_t>                  392922 ns       392917 ns         1785
BM_Iterate2D<Relocatable::PtrRelativizer::Vector, std::uint64_t>     392384 ns       392355 ns         1778
BM_Iterate2D<Relocatable::BoostOffsetPtr::Vector, std::uint64_t>     395175 ns       395170 ns         1766
BM_Iterate2D<Relocatable::OffsetPtr::Vector, std::uint64_t>          395213 ns       395198 ns         1773
```

### AMD Ryzen 7 7840U

```
2024-05-12T10:29:57+02:00
Running benchmark/benchmarkIterate2D
Run on (16 X 400 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x8)
  L1 Instruction 32 KiB (x8)
  L2 Unified 1024 KiB (x8)
  L3 Unified 16384 KiB (x1)
Load Average: 0.35, 0.38, 0.22
-----------------------------------------------------------------------------------------------------------
Benchmark                                                                 Time             CPU   Iterations
-----------------------------------------------------------------------------------------------------------
BM_Iterate2D<std::vector, std::uint8_t>                               14620 ns        14619 ns        47856
BM_Iterate2DBoostInterprocessVector<std::uint8_t>                    420108 ns       420085 ns         1667
BM_Iterate2D<NonRelocatable::Vector, std::uint8_t>                    16722 ns        16721 ns        41351
BM_Iterate2D<Relocatable::PtrRelativizer::Vector, std::uint8_t>       19018 ns        19017 ns        36796
BM_Iterate2D<Relocatable::BoostOffsetPtr::Vector, std::uint8_t>       22362 ns        22360 ns        34460
BM_Iterate2D<Relocatable::OffsetPtr::Vector, std::uint8_t>            21730 ns        21729 ns        35401
BM_Iterate2D<std::vector, std::uint32_t>                              58212 ns        58208 ns        12009
BM_Iterate2DBoostInterprocessVector<std::uint32_t>                   428009 ns       427985 ns         1636
BM_Iterate2D<NonRelocatable::Vector, std::uint32_t>                   79048 ns        79044 ns         8891
BM_Iterate2D<Relocatable::PtrRelativizer::Vector, std::uint32_t>      78979 ns        78973 ns         8853
BM_Iterate2D<Relocatable::BoostOffsetPtr::Vector, std::uint32_t>      59918 ns        59914 ns        11652
BM_Iterate2D<Relocatable::OffsetPtr::Vector, std::uint32_t>           59212 ns        59207 ns        11814
BM_Iterate2D<std::vector, std::uint64_t>                             109686 ns       109680 ns         6376
BM_Iterate2DBoostInterprocessVector<std::uint64_t>                   432688 ns       432657 ns         1618
BM_Iterate2D<NonRelocatable::Vector, std::uint64_t>                  111022 ns       111015 ns         6315
BM_Iterate2D<Relocatable::PtrRelativizer::Vector, std::uint64_t>     131514 ns       131505 ns         5320
BM_Iterate2D<Relocatable::BoostOffsetPtr::Vector, std::uint64_t>     112945 ns       112940 ns         6202
BM_Iterate2D<Relocatable::OffsetPtr::Vector, std::uint64_t>          111997 ns       111990 ns         6247
```


## RandomMap

### Intel XEON W3550

```
2024-05-12T09:43:50+02:00
Running benchmark/benchmarkRandomMap
Run on (4 X 3066.61 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x4)
  L1 Instruction 32 KiB (x4)
  L2 Unified 256 KiB (x4)
  L3 Unified 8192 KiB (x1)
Load Average: 0.29, 0.21, 0.08
--------------------------------------------------------------------------------------------------------
Benchmark                                                              Time             CPU   Iterations
--------------------------------------------------------------------------------------------------------
BM_RandomMap<std::map, std::uint8_t>                            26218016 ns     26212793 ns           27
BM_RandomBoostInterprocessMap<std::uint8_t>                     30007840 ns     30004614 ns           23
BM_RandomMap<NonRelocatable::Map, std::uint8_t>                 26814815 ns     26813019 ns           26
BM_RandomMap<Relocatable::BoostOffsetPtr::Map, std::uint8_t>    29533323 ns     29532293 ns           24
BM_RandomMap<Relocatable::OffsetPtr::Map, std::uint8_t>         27728241 ns     27726354 ns           25
BM_RandomMap<std::map, std::uint32_t>                           25990108 ns     25987916 ns           27
BM_RandomBoostInterprocessMap<std::uint32_t>                    30089907 ns     30087494 ns           23
BM_RandomMap<NonRelocatable::Map, std::uint32_t>                26296038 ns     26294593 ns           26
BM_RandomMap<Relocatable::BoostOffsetPtr::Map, std::uint32_t>   29659300 ns     29656925 ns           24
BM_RandomMap<Relocatable::OffsetPtr::Map, std::uint32_t>        28231172 ns     28229820 ns           25
BM_RandomMap<std::map, std::uint64_t>                           26345712 ns     26342392 ns           27
BM_RandomBoostInterprocessMap<std::uint64_t>                    30067970 ns     30064834 ns           23
BM_RandomMap<NonRelocatable::Map, std::uint64_t>                26454886 ns     26452184 ns           27
BM_RandomMap<Relocatable::BoostOffsetPtr::Map, std::uint64_t>   29637395 ns     29635353 ns           24
BM_RandomMap<Relocatable::OffsetPtr::Map, std::uint64_t>        27833598 ns     27832666 ns           25
```

### ARMv8, Broadcom BCM2711, Quad core Cortex-A72 (Raspberry Pi 4 Model B)

```
2024-05-12T09:50:42+02:00
Running benchmark/benchmarkRandomMap
Run on (4 X 1800 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x4)
  L1 Instruction 48 KiB (x4)
  L2 Unified 1024 KiB (x1)
Load Average: 0.41, 0.26, 0.11
--------------------------------------------------------------------------------------------------------
Benchmark                                                              Time             CPU   Iterations
--------------------------------------------------------------------------------------------------------
BM_RandomMap<std::map, std::uint8_t>                            90273730 ns     90269946 ns            8
BM_RandomBoostInterprocessMap<std::uint8_t>                    105030281 ns    105014941 ns            7
BM_RandomMap<NonRelocatable::Map, std::uint8_t>                101319537 ns    101313325 ns            9
BM_RandomMap<Relocatable::BoostOffsetPtr::Map, std::uint8_t>   105198262 ns    105195143 ns            7
BM_RandomMap<Relocatable::OffsetPtr::Map, std::uint8_t>        103728051 ns    103728399 ns            7
BM_RandomMap<std::map, std::uint32_t>                          101229473 ns    101229690 ns            7
BM_RandomBoostInterprocessMap<std::uint32_t>                   104728939 ns    104720981 ns            7
BM_RandomMap<NonRelocatable::Map, std::uint32_t>               101300413 ns    101287942 ns            9
BM_RandomMap<Relocatable::BoostOffsetPtr::Map, std::uint32_t>  105460725 ns    105460888 ns            7
BM_RandomMap<Relocatable::OffsetPtr::Map, std::uint32_t>       104074012 ns    104070423 ns            7
BM_RandomMap<std::map, std::uint64_t>                          101559779 ns    101498523 ns            8
BM_RandomBoostInterprocessMap<std::uint64_t>                   104916009 ns    104904518 ns            7
BM_RandomMap<NonRelocatable::Map, std::uint64_t>               102005286 ns    101997991 ns            9
BM_RandomMap<Relocatable::BoostOffsetPtr::Map, std::uint64_t>  107018924 ns    107006618 ns            7
BM_RandomMap<Relocatable::OffsetPtr::Map, std::uint64_t>       104742014 ns    104726597 ns            7
```

### AMD Ryzen 7 5700U

```
2024-05-13T07:56:03+02:00
Running ./benchmarkRandomMap
Run on (16 X 1865.4 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x8)
  L1 Instruction 32 KiB (x8)
  L2 Unified 512 KiB (x8)
  L3 Unified 4096 KiB (x2)
Load Average: 0.73, 0.29, 0.14
--------------------------------------------------------------------------------------------------------
Benchmark                                                              Time             CPU   Iterations
--------------------------------------------------------------------------------------------------------
BM_RandomMap<std::map, std::uint8_t>                            27283945 ns     27280467 ns           26
BM_RandomBoostInterprocessMap<std::uint8_t>                     27307636 ns     27307203 ns           26
BM_RandomMap<NonRelocatable::Map, std::uint8_t>                 20933562 ns     20930629 ns           33
BM_RandomMap<Relocatable::BoostOffsetPtr::Map, std::uint8_t>    22535140 ns     22535160 ns           31
BM_RandomMap<Relocatable::OffsetPtr::Map, std::uint8_t>         21342685 ns     21341561 ns           33
BM_RandomMap<std::map, std::uint32_t>                           24737589 ns     24736870 ns           28
BM_RandomBoostInterprocessMap<std::uint32_t>                    25096905 ns     25093577 ns           28
BM_RandomMap<NonRelocatable::Map, std::uint32_t>                20190522 ns     20189494 ns           34
BM_RandomMap<Relocatable::BoostOffsetPtr::Map, std::uint32_t>   21989241 ns     21986778 ns           32
BM_RandomMap<Relocatable::OffsetPtr::Map, std::uint32_t>        20585195 ns     20584170 ns           34
BM_RandomMap<std::map, std::uint64_t>                           25463427 ns     25462308 ns           28
BM_RandomBoostInterprocessMap<std::uint64_t>                    25096840 ns     25096013 ns           28
BM_RandomMap<NonRelocatable::Map, std::uint64_t>                20538154 ns     20536913 ns           34
BM_RandomMap<Relocatable::BoostOffsetPtr::Map, std::uint64_t>   22760777 ns     22757734 ns           30
BM_RandomMap<Relocatable::OffsetPtr::Map, std::uint64_t>        21168423 ns     21168007 ns           33
```

### AMD Ryzen 7 7840U

```
2024-05-12T10:30:43+02:00
Running benchmark/benchmarkRandomMap
Run on (16 X 400 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x8)
  L1 Instruction 32 KiB (x8)
  L2 Unified 1024 KiB (x8)
  L3 Unified 16384 KiB (x1)
Load Average: 0.32, 0.38, 0.23
--------------------------------------------------------------------------------------------------------
Benchmark                                                              Time             CPU   Iterations
--------------------------------------------------------------------------------------------------------
BM_RandomMap<std::map, std::uint8_t>                            12416336 ns     12418367 ns           56
BM_RandomBoostInterprocessMap<std::uint8_t>                     19052773 ns     19056097 ns           37
BM_RandomMap<NonRelocatable::Map, std::uint8_t>                 12515044 ns     12516903 ns           55
BM_RandomMap<Relocatable::BoostOffsetPtr::Map, std::uint8_t>    14273106 ns     14275553 ns           48
BM_RandomMap<Relocatable::OffsetPtr::Map, std::uint8_t>         13013128 ns     13014876 ns           54
BM_RandomMap<std::map, std::uint32_t>                           12089975 ns     12091727 ns           58
BM_RandomBoostInterprocessMap<std::uint32_t>                    19171352 ns     19173316 ns           36
BM_RandomMap<NonRelocatable::Map, std::uint32_t>                12553877 ns     12555248 ns           55
BM_RandomMap<Relocatable::BoostOffsetPtr::Map, std::uint32_t>   14589711 ns     14591173 ns           48
BM_RandomMap<Relocatable::OffsetPtr::Map, std::uint32_t>        13090292 ns     13091871 ns           52
BM_RandomMap<std::map, std::uint64_t>                           11642313 ns     11644010 ns           60
BM_RandomBoostInterprocessMap<std::uint64_t>                    19151174 ns     19153527 ns           37
BM_RandomMap<NonRelocatable::Map, std::uint64_t>                12813353 ns     12814890 ns           55
BM_RandomMap<Relocatable::BoostOffsetPtr::Map, std::uint64_t>   14817470 ns     14819353 ns           48
BM_RandomMap<Relocatable::OffsetPtr::Map, std::uint64_t>        13356765 ns     13358636 ns           52
```

## IterateMap

### Intel XEON W3550

```
2024-05-12T09:44:53+02:00
Running benchmark/benchmarkIterateMap
Run on (4 X 2268.25 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x4)
  L1 Instruction 32 KiB (x4)
  L2 Unified 256 KiB (x4)
  L3 Unified 8192 KiB (x1)
Load Average: 0.25, 0.23, 0.09
---------------------------------------------------------------------------------------------------------
Benchmark                                                               Time             CPU   Iterations
---------------------------------------------------------------------------------------------------------
BM_IterateMap<std::map, std::uint8_t>                             3332207 ns      3328259 ns          211
BM_IterateBoostInterprocessMap<std::uint8_t>                      4387102 ns      4385759 ns          160
BM_IterateMap<NonRelocatable::Map, std::uint8_t>                  2837294 ns      2836444 ns          247
BM_IterateMap<Relocatable::BoostOffsetPtr::Map, std::uint8_t>     3235240 ns      3234124 ns          216
BM_IterateMap<Relocatable::OffsetPtr::Map, std::uint8_t>          2895283 ns      2895173 ns          241
BM_IterateMap<std::map, std::uint32_t>                            3244120 ns      3243124 ns          216
BM_IterateBoostInterprocessMap<std::uint32_t>                     4282133 ns      4280867 ns          163
BM_IterateMap<NonRelocatable::Map, std::uint32_t>                 2851338 ns      2851164 ns          246
BM_IterateMap<Relocatable::BoostOffsetPtr::Map, std::uint32_t>    3227724 ns      3226893 ns          217
BM_IterateMap<Relocatable::OffsetPtr::Map, std::uint32_t>         2895946 ns      2894964 ns          242
BM_IterateMap<std::map, std::uint64_t>                            3216296 ns      3215435 ns          217
BM_IterateBoostInterprocessMap<std::uint64_t>                     4308410 ns      4306911 ns          163
BM_IterateMap<NonRelocatable::Map, std::uint64_t>                 2765930 ns      2764883 ns          253
BM_IterateMap<Relocatable::BoostOffsetPtr::Map, std::uint64_t>    3181519 ns      3180811 ns          220
BM_IterateMap<Relocatable::OffsetPtr::Map, std::uint64_t>         2839635 ns      2839473 ns          246
```

### ARMv8, Broadcom BCM2711, Quad core Cortex-A72 (Raspberry Pi 4 Model B)

```
2024-05-12T09:51:35+02:00
Running benchmark/benchmarkIterateMap
Run on (4 X 1800 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x4)
  L1 Instruction 48 KiB (x4)
  L2 Unified 1024 KiB (x1)
Load Average: 0.30, 0.26, 0.12
---------------------------------------------------------------------------------------------------------
Benchmark                                                               Time             CPU   Iterations
---------------------------------------------------------------------------------------------------------
BM_IterateMap<std::map, std::uint8_t>                            14729731 ns     14729178 ns           46
BM_IterateBoostInterprocessMap<std::uint8_t>                     16690552 ns     16688748 ns           42
BM_IterateMap<NonRelocatable::Map, std::uint8_t>                 13650710 ns     13650729 ns           51
BM_IterateMap<Relocatable::BoostOffsetPtr::Map, std::uint8_t>    14095945 ns     14095071 ns           50
BM_IterateMap<Relocatable::OffsetPtr::Map, std::uint8_t>         13648683 ns     13647623 ns           51
BM_IterateMap<std::map, std::uint32_t>                           14514902 ns     14510160 ns           47
BM_IterateBoostInterprocessMap<std::uint32_t>                    16684280 ns     16682842 ns           42
BM_IterateMap<NonRelocatable::Map, std::uint32_t>                13710183 ns     13709674 ns           51
BM_IterateMap<Relocatable::BoostOffsetPtr::Map, std::uint32_t>   14117792 ns     14114979 ns           49
BM_IterateMap<Relocatable::OffsetPtr::Map, std::uint32_t>        13686778 ns     13686186 ns           51
BM_IterateMap<std::map, std::uint64_t>                           14554203 ns     14552518 ns           48
BM_IterateBoostInterprocessMap<std::uint64_t>                    16688625 ns     16687823 ns           42
BM_IterateMap<NonRelocatable::Map, std::uint64_t>                13681866 ns     13680559 ns           51
BM_IterateMap<Relocatable::BoostOffsetPtr::Map, std::uint64_t>   14131450 ns     14130597 ns           49
BM_IterateMap<Relocatable::OffsetPtr::Map, std::uint64_t>        13670560 ns     13669491 ns           51
```

### AMD Ryzen 7 5700U

```
2024-05-13T07:55:09+02:00
Running ./benchmarkIterateMap
Run on (16 X 3527.31 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x8)
  L1 Instruction 32 KiB (x8)
  L2 Unified 512 KiB (x8)
  L3 Unified 4096 KiB (x2)
Load Average: 0.33, 0.15, 0.08
---------------------------------------------------------------------------------------------------------
Benchmark                                                               Time             CPU   Iterations
---------------------------------------------------------------------------------------------------------
BM_IterateMap<std::map, std::uint8_t>                             5433807 ns      5433239 ns          128
BM_IterateBoostInterprocessMap<std::uint8_t>                      5050689 ns      5050481 ns          139
BM_IterateMap<NonRelocatable::Map, std::uint8_t>                  3929792 ns      3929489 ns          177
BM_IterateMap<Relocatable::BoostOffsetPtr::Map, std::uint8_t>     4323480 ns      4323266 ns          161
BM_IterateMap<Relocatable::OffsetPtr::Map, std::uint8_t>          4118129 ns      4117976 ns          172
BM_IterateMap<std::map, std::uint32_t>                            5783199 ns      5782995 ns          121
BM_IterateBoostInterprocessMap<std::uint32_t>                     5921679 ns      5921592 ns          118
BM_IterateMap<NonRelocatable::Map, std::uint32_t>                 3988366 ns      3988305 ns          175
BM_IterateMap<Relocatable::BoostOffsetPtr::Map, std::uint32_t>    4384796 ns      4384542 ns          160
BM_IterateMap<Relocatable::OffsetPtr::Map, std::uint32_t>         4102139 ns      4101787 ns          169
BM_IterateMap<std::map, std::uint64_t>                            5978823 ns      5978235 ns          118
BM_IterateBoostInterprocessMap<std::uint64_t>                     5978763 ns      5977893 ns          118
BM_IterateMap<NonRelocatable::Map, std::uint64_t>                 3738683 ns      3738488 ns          188
BM_IterateMap<Relocatable::BoostOffsetPtr::Map, std::uint64_t>    4147269 ns      4146884 ns          169
BM_IterateMap<Relocatable::OffsetPtr::Map, std::uint64_t>         3834128 ns      3833839 ns          181
```

### AMD Ryzen 7 7840U

```
2024-05-12T10:31:23+02:00
Running benchmark/benchmarkIterateMap
Run on (16 X 400 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x8)
  L1 Instruction 32 KiB (x8)
  L2 Unified 1024 KiB (x8)
  L3 Unified 16384 KiB (x1)
Load Average: 0.34, 0.38, 0.24
---------------------------------------------------------------------------------------------------------
Benchmark                                                               Time             CPU   Iterations
---------------------------------------------------------------------------------------------------------
BM_IterateMap<std::map, std::uint8_t>                             1375882 ns      1376038 ns          509
BM_IterateBoostInterprocessMap<std::uint8_t>                      2490008 ns      2490284 ns          283
BM_IterateMap<NonRelocatable::Map, std::uint8_t>                  1436437 ns      1436592 ns          487
BM_IterateMap<Relocatable::BoostOffsetPtr::Map, std::uint8_t>     1680889 ns      1681023 ns          418
BM_IterateMap<Relocatable::OffsetPtr::Map, std::uint8_t>          1523296 ns      1523433 ns          460
BM_IterateMap<std::map, std::uint32_t>                            1363345 ns      1363488 ns          513
BM_IterateBoostInterprocessMap<std::uint32_t>                     2486194 ns      2486355 ns          283
BM_IterateMap<NonRelocatable::Map, std::uint32_t>                 1448326 ns      1448460 ns          487
BM_IterateMap<Relocatable::BoostOffsetPtr::Map, std::uint32_t>    1677044 ns      1677185 ns          417
BM_IterateMap<Relocatable::OffsetPtr::Map, std::uint32_t>         1512857 ns      1513004 ns          464
BM_IterateMap<std::map, std::uint64_t>                            1365683 ns      1365815 ns          511
BM_IterateBoostInterprocessMap<std::uint64_t>                     2493951 ns      2494108 ns          282
BM_IterateMap<NonRelocatable::Map, std::uint64_t>                 1440325 ns      1440444 ns          486
BM_IterateMap<Relocatable::BoostOffsetPtr::Map, std::uint64_t>    1674717 ns      1674870 ns          417
BM_IterateMap<Relocatable::OffsetPtr::Map, std::uint64_t>         1500805 ns      1500951 ns          467
```
