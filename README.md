# TrivialityInversion

A proof of concept for storage location agnostic C++ container types.

## Prior art

The idea foundation is very similar to [Boost offset pointers](https://www.boost.org/doc/libs/1_84_0/doc/html/interprocess/offset_ptr.html).

## POC scope

A rudimentary and partial clone of `std::vector`, just enough to explore its storage dependent properties. The prerequisites for extending the approach to more complex types (map, list, ...) are provided.

## Design goal

The C++ polymorphic memory resource framework using `std::pmr::polymorphic_allocator<T>` has made storage RAII containers (vector, string, map, ...) transparent regarding their underlying memory resources. By using `std::pmr`, container references can be used at any interface without depending on where and how the actual payload memory was allocated.

This POC attempts to extend this transparency to process storage categories: heap, stack, shared memory.

### The issue

Shared memory is most challenging, as the same binary object representation is mapped into different process virtual address spaces with different offsets. Consequently, pointer values valid in one virtual address space are invalid in all others. For being compatible with shared memory, classes

 * must not use pointer values in their object state,
 * and can't have virtual functions, as these include and use a vtable pointer as a hidden member.

Note that this restriction does not apply to pointer local variables of class member functions. Their visiblity is limited to the process running a function and never placed in shared memory.

The idea is to represent required pointer members by offsets relative to the this-pointer. This kind of addressing works with pointer targets both within and outside the object.

## Design

### Option `OffsetPtr`: use offsets relative to the pointer object

```plantuml
skinparam style strictuml

class OffsetPtr<T> {
        offset_ : std::intptr_t
        OffsetPtr(T const*)
        operator T*()
        operator T const*() const
}

class NullableOffsetPtr<T> {
        offset_ : std::intptr_t
        OffsetPtr(T const*)
        operator T*()
        operator T const*() const
}

class Vector<T, Alloc> {
        storage_ : OffsetPtr<T>
        Vector(Alloc const&)
        Vector(BufferView const&)
}

class Map<Key, T, Compare, Alloc> {
        free_ : NullableOffsetPtr<Node>
        root_ : NullableOffsetPtr<Node>
        Map(Alloc const&)
        Map(BufferView const&)
}

Vector *-up- OffsetPtr
Map *-up- NullableOffsetPtr
```

Similar to [Boost offset pointers](https://www.boost.org/doc/libs/1_84_0/doc/html/interprocess/offset_ptr.html), but vastly simplified by just doing offset calculation and pointer type conversions. The offset class representations do not provide any pointer behavior on their own.

### Option `PtrRelativizer`: use offsets relative to the owner object

```plantuml
skinparam style strictuml

class PtrRelativizer {
        toPtr(RelativePtr<T> const*) : T*
        toPtr(NullableRelativePtr<T> const*) : T*
}

class PtrRelativizer::RelativePtr<T> {
        offset_ : std::intptr_t
        RelativePtr(PtrRelativizer const&, T const*)
}

class PtrRelativizer::NullableRelativePtr<T> {
        offset_ : std::intptr_t
        NullableRelativePtr(PtrRelativizer const&, T const*)
}

class Vector<T, Alloc> {
        storage_ : PtrRelativizer::RelativePtr<T>
        Vector(Alloc const&)
        Vector(BufferView const&)
}

class Map<Key, T, Compare, Alloc> {
        free_ : PtrRelativizer::NullableRelativePtr<Node>
        root_ : PtrRelativizer::NullableRelativePtr<Node>
        Map(Alloc const&)
        Map(BufferView const&)
}

Vector -up-|> PtrRelativizer : extends
Vector *-up- PtrRelativizer::RelativePtr
Map -up-|> PtrRelativizer : extends
Map *-up- PtrRelativizer::NullableRelativePtr
PtrRelativizer .left. PtrRelativizer::RelativePtr : T* conversion
PtrRelativizer .right. PtrRelativizer::NullableRelativePtr : T* conversion
```

Classes being placed in any kind of memory shall inherit privately from `PtrRelativizer`. This base class provides a public inner class template `PtrRelativizer::RelativePtr<T>`, composing the offset representation of a `T*`.

Private inheritance

 * prevents external references to the base class, eliminating the need to provide a base class virtual destructor,
 * enables dereference functions for `PtrRelativizer::RelativePtr<T>` by sharing the this-pointer with the derived class.

### Fixed capacity objects

In both shared memory and stack storage the payload storage must be stored along with the container object, and is of fixed size. The container class templates provide constructor overloads, accepting a `BufferView` object instead of an allocator object, which describes the associated buffer's location and size. The view is used to initialize the container internal relative pointer members. A dedicated `BufferView` is preferrable over more generic types like `std::span`, or over separate location and size parameters, to avoid potential overload ambiguities with constructor arguments initializing the payload.

Note that the set of template parameters is *not* changed to distinguish static from dynamic storage. It still has the `Alloc = std::pmr::polymorphic_allocator<T>` parameter, and the container class remains the same type. Mentioned static buffer constructors initializes the allocator member with a default constructed allocator object for compatibility, but will not use it. In case of `std::pmr::polymorphic_allocator<T>` this imposes overhead of a single memory resource pointer being stored, but not used.

### Triviality inversion: relocatable, but not trivially copyable?

The combination of container and buffer for common instantiation in shared memory or on stack may look like

```
template<typename T, std::size_t size>
struct BoundedVector
{
	BoundedVector() = default;

	Vector<T> v{typename Vector<T>::BufferView{buffer.data(), buffer.size()}};
	alignas(T) std::array<std::byte, size*(sizeof(T) + alignof(T) - 1 - (sizeof(T) - 1) % alignof(T))> buffer;
};

template<typename Key, typename T, typename Compare, std::size_t size>
struct BoundedMap
{
	BoundedMap() = default;

	Map<Key, T, Compare> m{typename Map<Key, T, Compare>::BufferView{buffer.data(), buffer.size()}};
	alignas(Map<Key, T, Compare>::alignofNode) std::array<std::byte, size * Map<Key, T, Compare>::sizeofNode> buffer;
};
```

Its member object of type `Vector<T>` is not trivially copyable. That absolutely requires a non-trivial copy constructor, just like `std::vector<T>` does. Otherwise a copy operation wouldn't include content as expected, but just create a new view in case of `NonRelocatable::Vector<T>` does, or point into the wild in case of `Relocatable::*::Vector`.

By C++ rules, the surrounding `BoundedVector<T, size>` can't be trivially copyable, either, due to its non-trivially copyable member `v`. However, it *effectively* is! You could memcpy its contents to a different location and produce a fully functional object, which is equivalent to mapping a shared memory object to a different address. This results from `BoundedVector<T, size>` also being self-contained. `BoundedVector<T, size>::v` does have a storage pointer targetting outside its object, but `BoundedVector<T, size>` in total doesn't.

![Triviality inversion](doc/TrivialityInversion.svg)

In other words: A non-trivially copyable object becomes *effectively* trivially copyable by being bundled with another object, the "landing zone" for all its external references.

### Copying effectively trivially copyable objects

Let's consider regular copies, not done with a memcpy or by mapping pre-populated shared memory to a different offset.

The limitation of effective triviality, as opposed to real triviality, implies that doing an actual copy from one object to another is not trivial. A `BoundedVector` class will have a non-trivial copy constructor and assignment operator, regardless whether it is the compiler generated default special function, or any user defined copy constructor. The default will work, but probably not the way one might trivially expect:

1. `BoundedVector(BoundedVector const&)` invokes
2. `Vector<T>::Vector(Vector const&)`, which in turn populates
3. member `BoundedVector::v` of the target object, but initializing its allocator to retrieve memory from `std::pmr::get_default_resource()`, then copying content to allocated heap memory.
4. `std::array<std::byte, std::size_t>::array(array const&)`, which will copy contents again, but to the target `BoundedVector::buffer`

Test case [`TEST(BoundedVector, Copy)`](https://gitlab.com/misteffens/TrivialityInversion/-/blob/main/test/testBoundedVector.cpp?ref_type=heads) demonstrates this effect.

Having two deep copies in 3. and 4., with the target buffer finally being orphaned, is useless. It would make more sense to use `Vector<T>::Vector(Vector const&, BufferView const&)` in step 2. and link the target vector to the target buffer, and have content copied there, finally skip the paylod copy of the buffer object. You could do so in an explicit copy constructor `BoundedVector`. On the other hand, since a class can have only one copy constructor, which is *intended* to match C++ standard semantics in case of `Vector`, fixing bounded object copies in `Vector` is not an option.

Conclusion: When copying effectively trivially copyable classes like `BoundedVector`, provide explicit copy constructors. Or delete them.

## Undefined behavior aspects

Do the `reinterpret_cast` operations in `OffsetPtr`, `NullableOffsetPtr`, `PtrRelativizer` and its members `RelativePtr` and `NullableRelativePtr` introduce undefined behavior? [reinterpret_cast explanation item 3](https://en.cppreference.com/w/cpp/language/reinterpret_cast#Explanation) states

> A value of any integral or enumeration type can be converted to a pointer type.
> **A pointer converted to an integer of sufficient size and back to the same pointer
> type is guaranteed to have its original value**, otherwise the resulting pointer
> cannot be dereferenced safely (the round-trip conversion in the opposite direction
> is not guaranteed; the same pointer may have multiple integer representations)

This round-trip condition is met, as offset pointer construction and conversions back into raw pointers depends on

```
reinterpret_cast<intptr_t>(ptr) - reinterpret_cast<intptr_t>(this) + reinterpret_cast<intptr_t>(this) == reinterpret_cast<intptr_t>(ptr)
```

which is well defined and true for all integral arithmetics. Note that this would not cover navigating to any other pointer target object, for example by incrementing or decrementing offset. None of the classes above provides such navigation based on internal computation.

## Performance and memory footprint

While not zero, the impact is expected to be small:

 * Dereferencing the relative pointer is equivalent to an index access using a local member array, which is usually very efficient.
 * A boolean flag is added to the container state. It is used to prevent capacity modification after construction, when using static buffer storage. This applies to both stack and shared memory storage, and works with any offset of shared memory addresses.

See [benchmark](https://gitlab.com/misteffens/TrivialityInversion/-/tree/main/benchmark?ref_type=heads) folder.

## Testing and POC

See [test](https://gitlab.com/misteffens/TrivialityInversion/-/tree/main/test?ref_type=heads) folder.

## Author

Dr. Michael Steffens, 2024
