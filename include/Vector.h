//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#ifndef Vector_h_INCLUDED
#define Vector_h_INCLUDED

#include "NonRelocatable/Vector.h"
#include "Relocatable/BoostOffsetPtr/Vector.h"
#include "Relocatable/OffsetPtr/Vector.h"
#include "Relocatable/PtrRelativizer/Vector.h"

template<typename T, typename Alloc = std::pmr::polymorphic_allocator<T>>
using Vector = Relocatable::OffsetPtr::Vector<T, Alloc>;

#endif // Vector_h_INCLUDED
