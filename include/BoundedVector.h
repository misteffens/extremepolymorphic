//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#ifndef BoundedVector_h_INCLUDED
#define BoundedVector_h_INCLUDED

#include <Vector.h>
#include <array>
#include <type_traits>

template<typename T, std::size_t size>
struct BoundedVector
{
	BoundedVector() = default;
	// BoundedVector(BoundedVector const&) = delete;
	// BoundedVector& operator=(BoundedVector const&) = delete;

	Vector<T> v{typename Vector<T>::BufferView{buffer.data(), buffer.size()}};
	// Aligned size of T: (sizeof(T) + alignof(T) - 1 - (sizeof(T) - 1) % alignof(T)) is the T* increment in bytes.
	alignas(T) std::array<std::byte, size * (sizeof(T) + alignof(T) - 1 - (sizeof(T) - 1) % alignof(T))> buffer;
};

// Not trivially copyable, despite it effectively is for trivially copyable T, when using Relocatable::PtrRelativizer::Vector<T>!
static_assert(!std::is_trivially_copyable<BoundedVector<std::byte, 0>>::value);

#endif // BoundedVector_h_INCLUDED
