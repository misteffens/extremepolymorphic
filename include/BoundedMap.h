//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#ifndef BoundedMap_h_INCLUDED
#define BoundedMap_h_INCLUDED

#include <Map.h>
#include <array>
#include <type_traits>

template<typename Key, typename T, typename Compare, std::size_t size>
struct BoundedMap
{
	BoundedMap() = default;
	// BoundedMap(BoundedMap const&) = delete;
	// BoundedMap& operator=(BoundedMap const&) = delete;

	Map<Key, T, Compare> m{typename Map<Key, T, Compare>::BufferView{buffer.data(), buffer.size()}};
	alignas(Map<Key, T, Compare>::alignofNode) std::array<std::byte, size * Map<Key, T, Compare>::sizeofNode> buffer;
};

// Not trivially copyable, despite it effectively is for trivially copyable T, when using Relocatable::PtrRelativizer::Map<Key, T,
// Compare>!
static_assert(!std::is_trivially_copyable<BoundedMap<int, int, std::less<int>, 0>>::value);

#endif // BoundedMap_h_INCLUDED
