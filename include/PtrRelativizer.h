//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#ifndef PtrRelativizer_h_INCLUDED
#define PtrRelativizer_h_INCLUDED

#include <cstddef>

class PtrRelativizer
{
public:
	// Least overhead variant, that does not represent NULL pointer consistenly with different virtual
	// address offsets. Appropriate when the NULL value is not needed, such as container main storage
	// pointers, where absence of storage can be represented by zero capacity.
	template<typename T>
	class RelativePtr
	{
	public:
		// Copy and assignment semantics across owners impossible without knowing the owner
		// base adresses. Operations must therefor be performed by the owner instead.
		// Move is deleted implicitly;
		RelativePtr(RelativePtr const&) = delete;
		RelativePtr& operator=(RelativePtr const&) = delete;

		RelativePtr(PtrRelativizer const& owner, T const* ptr) :
			offset_{reinterpret_cast<std::byte const*>(ptr) - reinterpret_cast<std::byte const*>(&owner)}
		{
		}

		void assign(PtrRelativizer const& owner, T const* ptr)
		{
			offset_ = reinterpret_cast<std::byte const*>(ptr) - reinterpret_cast<std::byte const*>(&owner);
		}

	private:
		friend class PtrRelativizer;
		std::ptrdiff_t offset_; // Using units of bytes, as alignments of *this and *ptr may not match.
	};

	// Nullable variant, imposing overhead in memory for the isNull flag, and in runtime for NULL pointer
	// tests on assignment and dereferencing. Appropriate for container internal node data structures in
	// storage, where NULL pointers are needed to indicate leaf nodes. Examples: map, list.
	template<typename T>
	class NullableRelativePtr
	{
	public:
		// Copy and assignment semantics across owners impossible without knowing the owner
		// base adresses. Operations must therefor be performed by the owner instead.
		// Move is deleted implicitly;
		NullableRelativePtr(NullableRelativePtr const&) = delete;
		NullableRelativePtr& operator=(NullableRelativePtr const&) = delete;

		NullableRelativePtr(PtrRelativizer& owner, T* ptr) :
			offset_{reinterpret_cast<std::byte const*>(ptr) - reinterpret_cast<std::byte const*>(&owner)}, isNull_{ptr == nullptr}
		{
		}

		void assign(PtrRelativizer& owner, T* ptr)
		{
			offset_ = reinterpret_cast<std::byte const*>(ptr) - reinterpret_cast<std::byte const*>(&owner);
			isNull_ = ptr == nullptr;
		}

	private:
		friend class PtrRelativizer;
		std::ptrdiff_t offset_; // Using units of bytes, as alignments of *this and *ptr may not match.
		bool isNull_;
	};

	template<typename T>
	T* toPtr(RelativePtr<T> const& ptr)
	{
		return reinterpret_cast<T*>(reinterpret_cast<std::byte*>(this) + ptr.offset_);
	}

	template<typename T>
	T const* toPtr(RelativePtr<T> const& ptr) const
	{
		return reinterpret_cast<T const*>(reinterpret_cast<std::byte const*>(this) + ptr.offset_);
	}

	template<typename T>
	T* toPtr(NullableRelativePtr<T>& ptr)
	{
		return ptr.isNull_ ? nullptr : reinterpret_cast<T*>(reinterpret_cast<std::byte*>(this) + ptr.offset_);
	}

	template<typename T>
	T const* toPtr(NullableRelativePtr<T> const& ptr) const
	{
		return ptr.isNull_ ? nullptr : reinterpret_cast<T const*>(reinterpret_cast<std::byte const*>(this) + ptr.offset_);
	}
};

#endif // PtrRelativizer_h_INCLUDED
