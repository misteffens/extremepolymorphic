//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#ifndef Relocatable_BoostOffsetPtr_Map_h_INCLUDED
#define Relocatable_BoostOffsetPtr_Map_h_INCLUDED

#include <boost/interprocess/offset_ptr.hpp>
#include <cstddef>
#include <functional>
#include <initializer_list>
#include <memory_resource>
#include <stdexcept>
#include <utility>

namespace Relocatable { namespace BoostOffsetPtr {

template<
	typename Key,
	typename T,
	typename Compare = std::less<Key>,
	typename Alloc = std::pmr::polymorphic_allocator<std::pair<const Key, T>>>
class Map
{
	struct Node;

public:
	using value_type = std::pair<const Key, T>;

	class iterator
	{
	public:
		iterator() : node_{nullptr}
		{
		}

		explicit iterator(Node* node) : node_{node}
		{
		}

		iterator& operator++()
		{
			if (node_->gt_ != nullptr) {
				node_ = node_->gt_.get();
				while (node_->lt_ != nullptr) {
					node_ = node_->lt_.get();
				}
			} else {
				Node* ptr{node_->parent_.get()};
				while (ptr != nullptr && node_ == ptr->gt_) {
					node_ = ptr;
					ptr = ptr->parent_.get();
				}
				node_ = ptr;
			}
			return *this;
		}

		iterator operator++(int)
		{
			iterator result{*this};
			operator++();
			return result;
		}

		bool operator==(iterator const& other) const
		{
			return node_ == other.node_;
		}

		bool operator!=(iterator const& other) const
		{
			return !(*this == other);
		}

		value_type& operator*() const
		{
			return node_->value_;
		}

		value_type* operator->() const
		{
			return &node_->value_;
		}

	private:
		Node* node_;
	};

	struct BufferView
	{
		void* data;
		std::size_t bytes;
	};

	static constexpr std::size_t sizeofNode{sizeof(Node) + alignof(Node) - 1 - (sizeof(Node) - 1) % alignof(Node)};
	static constexpr std::size_t alignofNode{alignof(Node)};

	explicit Map(Alloc const& alloc = Alloc{}) : alloc_{alloc}, nodeAlloc_{alloc_}, hasConstCapacity_{false}
	{
	}

	explicit Map(BufferView const& buffer) : alloc_{Alloc{}}, nodeAlloc_{alloc_}, hasConstCapacity_{true}
	{
		std::size_t const capacity{buffer.bytes / sizeofNode};
		if (capacity > 0) {
			Node* node{reinterpret_cast<Node*>(buffer.data)};
			free_ = node;
			for (std::size_t i{0}; i < capacity - 1; ++i) {
				std::allocator_traits<NodeAlloc>::construct(
					nodeAlloc_, node, Node{.lt_ = nullptr, .gt_ = nullptr, .parent_ = node + 1});
				++node;
			}
			std::allocator_traits<NodeAlloc>::construct(nodeAlloc_, node, Node{.lt_ = nullptr, .gt_ = nullptr, .parent_ = nullptr});
		}
	}

	~Map()
	{
		clear();
		if (hasConstCapacity_) {
			Node* node{free_.get()};
			while (node != nullptr) {
				Node* dispose{node};
				node = node->parent_.get();
				std::allocator_traits<NodeAlloc>::destroy(nodeAlloc_, dispose);
			}
		}
	}

	iterator begin()
	{
		Node* node{nullptr};
		for (Node* ptr{root_.get()}; ptr != nullptr; ptr = ptr->lt_.get()) {
			node = ptr;
		}
		return iterator{node};
	}

	iterator end()
	{
		return iterator{};
	}

	std::pair<iterator, bool> insert(value_type const& value)
	{
		std::pair<iterator, bool> result{iterator{}, false};
		if (root_ != nullptr) {
			std::pair<Node*, Match> match{lookup(value.first)};
			switch (match.second) {
			case Match::lt:
				match.first->lt_ = create(match.first, value);
				result = std::pair<iterator, bool>{iterator{match.first->lt_.get()}, true};
				break;
			case Match::gt:
				match.first->gt_ = create(match.first, value);
				result = std::pair<iterator, bool>{iterator{match.first->gt_.get()}, true};
				break;
			default: // Match::eq
				result = std::pair<iterator, bool>{iterator{match.first}, false};
			}
		} else {
			root_ = create(nullptr, value);
			result = std::pair<iterator, bool>{iterator{root_.get()}, true};
		}
		return result;
	}

	T& operator[](Key const& key)
	{
		T* result;
		if (root_ != nullptr) {
			std::pair<Node*, Match> match{lookup(key)};
			switch (match.second) {
			case Match::lt:
				match.first->lt_ = create(match.first, value_type{key, T{}});
				result = &match.first->lt_->value_.second;
				break;
			case Match::gt:
				match.first->gt_ = create(match.first, value_type{key, T{}});
				result = &match.first->gt_->value_.second;
				break;
			default: // Match::eq
				result = &match.first->value_.second;
			}
		} else {
			root_ = create(nullptr, value_type{key, T{}});
			result = &root_->value_.second;
		}
		return *result;
	}

	T& at(Key const& key)
	{
		if (root_ != nullptr) {
			std::pair<Node*, Match> match{lookup(key)};
			if (match.second == Match::eq) {
				return match.first->value_.second;
			}
		}
		throw std::out_of_range{"NonRelocatable::Map<Key, T, Compare, Alloc>::at(Key const&)"};
	}

	T const& at(Key const& key) const
	{
		if (root_ != nullptr) {
			std::pair<Node*, Match> match{lookup(key)};
			if (match.second == Match::eq) {
				return match.first->value_.second;
			}
		}
		throw std::out_of_range{"NonRelocatable::Map<Key, T, Compare, Alloc>::at(Key const&) const"};
	}

	void clear()
	{
		Node* node{root_.get()};
		while (node != nullptr) {
			for (Node* ptr{node}; ptr != nullptr; ptr = ptr->lt_.get()) {
				node = ptr;
			}
			std::allocator_traits<Alloc>::destroy(alloc_, &node->value_);
			if (node->parent_ != nullptr) {
				node->parent_->lt_ = node->gt_;
			} else {
				root_ = node->gt_;
			}
			Node* dispose{node};
			if (node->gt_ != nullptr) {
				node->gt_->parent_ = node->parent_;
				node = node->gt_.get();
			} else {
				node = node->parent_.get();
			}
			if (hasConstCapacity_) {
				dispose->parent_ = free_;
				free_ = dispose;
			} else {
				std::allocator_traits<NodeAlloc>::destroy(nodeAlloc_, dispose);
				std::allocator_traits<NodeAlloc>::deallocate(nodeAlloc_, dispose, 1);
			}
			--size_;
		}
	}

	std::size_t size() const
	{
		return size_;
	}

private:
	struct Node
	{
		boost::interprocess::offset_ptr<Node> lt_;
		boost::interprocess::offset_ptr<Node> gt_;
		boost::interprocess::offset_ptr<Node> parent_;
		union
		{
			std::array<std::byte, sizeof(value_type)> storage_;
			value_type value_;
		};
	};

	using NodeAlloc = typename std::allocator_traits<Alloc>::template rebind_alloc<Node>;

	enum Match
	{
		eq,
		lt,
		gt
	};

	std::pair<Node*, Match> lookup(Key const& key) const
	{
		Compare compare{};
		std::pair<Node*, Match> result{const_cast<Node*>(root_.get()), Match::eq};
		while (true) {
			if (compare(key, result.first->value_.first)) {
				if (result.first->lt_ == nullptr) {
					result.second = Match::lt;
					break;
				}
				result.first = result.first->lt_.get();
			} else if (compare(result.first->value_.first, key)) {
				if (result.first->gt_ == nullptr) {
					result.second = Match::gt;
					break;
				}
				result.first = result.first->gt_.get();
			} else {
				break;
			}
		}
		return result;
	}

	Node* create(Node* parent, value_type value)
	{
		Node* result;
		if (hasConstCapacity_) {
			if (free_ != nullptr) {
				result = free_.get();
				free_ = free_->parent_;
				result->lt_ = nullptr;
				result->gt_ = nullptr;
				result->parent_ = parent;
			} else {
				throw std::bad_alloc{};
			}
		} else {
			result = std::allocator_traits<NodeAlloc>::allocate(nodeAlloc_, 1);
			std::allocator_traits<NodeAlloc>::construct(
				nodeAlloc_, result, Node{.lt_ = nullptr, .gt_ = nullptr, .parent_ = parent});
		}
		std::allocator_traits<Alloc>::construct(alloc_, &result->value_, value);
		++size_;
		return result;
	}

	std::size_t size_{0};
	Alloc alloc_;
	NodeAlloc nodeAlloc_;
	boost::interprocess::offset_ptr<Node> root_{nullptr};
	boost::interprocess::offset_ptr<Node> free_{nullptr};
	bool const hasConstCapacity_;
};

}} // namespace Relocatable::BoostOffsetPtr

#endif // Relocatable_BoostOffsetPtr_Map_h_INCLUDED
