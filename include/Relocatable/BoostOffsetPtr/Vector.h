//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#ifndef Relocatable_BoostOffsetPtr_Vector_h_INCLUDED
#define Relocatable_BoostOffsetPtr_Vector_h_INCLUDED

#include <boost/interprocess/offset_ptr.hpp>
#include <initializer_list>
#include <memory_resource>

namespace Relocatable { namespace BoostOffsetPtr {

template<typename T, typename Alloc = std::pmr::polymorphic_allocator<T>>
class Vector
{
public:
	using iterator = T*;
	using const_iterator = T const*;

	struct BufferView
	{
		void* data;
		std::size_t bytes;
	};

	explicit Vector(Alloc const& alloc = Alloc{}) : alloc_{alloc}, capacity_{0}, storage_{nullptr}, hasConstCapacity_{false}
	{
	}

	explicit Vector(BufferView const& buffer) :
		alloc_{Alloc{}},
		capacity_{buffer.bytes / (sizeof(T) + alignof(T) - 1 - (sizeof(T) - 1) % alignof(T))},
		storage_{reinterpret_cast<T*>(buffer.data)},
		hasConstCapacity_{true}
	{
	}

	explicit Vector(std::initializer_list<T> init, Alloc const& alloc = Alloc{}) : Vector{init.begin(), init.end(), alloc}
	{
	}

	explicit Vector(std::initializer_list<T> init, BufferView const& buffer) : Vector{init.begin(), init.end(), buffer}
	{
	}

	Vector(Vector const& other) : Vector{other.begin(), other.end(), other.get_allocator()}
	{
	}

	Vector(Vector const& other, BufferView const& buffer) : Vector{other.begin(), other.end(), buffer}
	{
	}

	template<typename InputIt>
	Vector(InputIt first, InputIt last, Alloc const& alloc = Alloc{}) :
		alloc_{alloc},
		capacity_(last - first),
		storage_{std::allocator_traits<Alloc>::allocate(alloc_, capacity_)},
		hasConstCapacity_{false}
	{
		while (first != last) {
			std::allocator_traits<Alloc>::construct(alloc_, &storage_[size_++], *first++);
		}
	}

	template<typename InputIt>
	Vector(InputIt first, InputIt last, BufferView const& buffer) :
		alloc_{Alloc{}},
		capacity_{buffer.bytes / (sizeof(T) + alignof(T) - 1 - (sizeof(T) - 1) % alignof(T))},
		storage_{reinterpret_cast<T*>(buffer.data)},
		hasConstCapacity_{true}
	{
		if (last - first > capacity_) {
			throw std::bad_alloc{};
		}
		while (first != last) {
			std::allocator_traits<Alloc>::construct(alloc_, &storage_[size_++], *first++);
		}
	}

	~Vector()
	{
		if (capacity_ > 0) {
			clear();
			if (!hasConstCapacity_) {
				std::allocator_traits<Alloc>::deallocate(alloc_, storage_.get(), capacity_);
			}
		}
	}

	void reserve(std::size_t n)
	{
		if (n > capacity_) {
			if (hasConstCapacity_) {
				throw std::bad_alloc{};
			}
			T* newStorage{std::allocator_traits<Alloc>::allocate(alloc_, n)};
			if (capacity_ > 0) {
				for (std::size_t i{0}; i != size_; ++i) {
					std::allocator_traits<Alloc>::construct(alloc_, &newStorage[i], storage_[i]);
					std::allocator_traits<Alloc>::destroy(alloc_, &storage_[i]);
				}
				std::allocator_traits<Alloc>::deallocate(alloc_, storage_.get(), capacity_);
			}
			storage_ = newStorage;
			capacity_ = n;
		}
	}

	iterator begin()
	{
		return storage_.get();
	}

	const_iterator begin() const
	{
		return storage_.get();
	}


	iterator end()
	{
		return storage_.get() + size_;
	}

	const_iterator end() const
	{
		return storage_.get() + size_;
	}

	Alloc get_allocator() const
	{
		return alloc_;
	}

	std::size_t capacity() const
	{
		return capacity_;
	}

	std::size_t size() const
	{
		return size_;
	}

	T& operator[](std::size_t pos)
	{
		return storage_[pos];
	}

	T const& operator[](std::size_t pos) const
	{
		return storage_[pos];
	}

	T* data()
	{
		return storage_.get();
	}

	T const* data() const
	{
		return storage_.get();
	}

	void clear()
	{
		while (size_ > 0) {
			std::allocator_traits<Alloc>::destroy(alloc_, &storage_[--size_]);
		}
	}

	void assign(std::initializer_list<T> init)
	{
		assign(init.begin(), init.end());
	}

	template<typename InputIt>
	void assign(InputIt first, InputIt last)
	{
		clear();
		reserve(last - first);
		while (first != last) {
			std::allocator_traits<Alloc>::construct(alloc_, &storage_[size_++], *first++);
		}
	}

	void push_back(T const& value)
	{
		if (size_ == capacity_) {
			reserve(size_ + 1);
		}
		std::allocator_traits<Alloc>::construct(alloc_, &storage_[size_++], value);
	}

private:
	std::size_t size_{0};
	Alloc alloc_;
	std::size_t capacity_;
	boost::interprocess::offset_ptr<T> storage_;
	bool const hasConstCapacity_;
};

}} // namespace Relocatable::BoostOffsetPtr

#endif // Relocatable_BoostOffsetPtr_Vector_h_INCLUDED
