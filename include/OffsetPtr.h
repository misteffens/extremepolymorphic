//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#ifndef OffsetPtr_h_INCLUDED
#define OffsetPtr_h_INCLUDED

#include <cstdint>

template<typename T>
class OffsetPtr
{
public:
	OffsetPtr(T const* ptr) : offset_{toInt(ptr) - toInt(this)}
	{
	}

	OffsetPtr(OffsetPtr const& other) : offset_{toInt(static_cast<T const*>(other)) - toInt(this)}
	{
	}

	OffsetPtr& operator=(OffsetPtr const& other)
	{
		offset_ = toInt(static_cast<T const*>(other)) - toInt(this);
		return *this;
	}

	T* operator->()
	{
		return toPtr(toInt(this) + offset_);
	}

	T const* operator->() const
	{
		return toConstPtr(toInt(this) + offset_);
	}

	operator T*()
	{
		return toPtr(toInt(this) + offset_);
	}

	operator T const*() const
	{
		return toConstPtr(toInt(this) + offset_);
	}

private:
	static std::intptr_t toInt(void const* ptr)
	{
		return reinterpret_cast<std::intptr_t>(ptr);
	}

	static T* toPtr(std::intptr_t n)
	{
		return static_cast<T*>(reinterpret_cast<void*>(n));
	}

	static T const* toConstPtr(std::intptr_t n)
	{
		return static_cast<T const*>(reinterpret_cast<void*>(n));
	}

	std::intptr_t offset_;
};

template<typename T>
class NullableOffsetPtr
{
public:
	NullableOffsetPtr(T const* ptr) : offset_{toInt(ptr) - toInt(this)}, isNull_{ptr == nullptr}
	{
	}

	NullableOffsetPtr(NullableOffsetPtr const& other) :
		offset_{toInt(static_cast<T const*>(other)) - toInt(this)}, isNull_{static_cast<T const*>(other) == nullptr}
	{
	}

	NullableOffsetPtr& operator=(NullableOffsetPtr const& other)
	{
		offset_ = toInt(static_cast<T const*>(other)) - toInt(this);
		isNull_ = static_cast<T const*>(other) == nullptr;
		return *this;
	}

	T* operator->()
	{
		return isNull_ ? nullptr : toPtr(toInt(this) + offset_);
	}

	T const* operator->() const
	{
		return isNull_ ? nullptr : toConstPtr(toInt(this) + offset_);
	}

	operator T*()
	{
		return isNull_ ? nullptr : toPtr(toInt(this) + offset_);
	}

	operator T const*() const
	{
		return isNull_ ? nullptr : toConstPtr(toInt(this) + offset_);
	}

private:
	static std::intptr_t toInt(void const* ptr)
	{
		return reinterpret_cast<std::intptr_t>(ptr);
	}

	static T* toPtr(std::intptr_t n)
	{
		return static_cast<T*>(reinterpret_cast<void*>(n));
	}

	static T const* toConstPtr(std::intptr_t n)
	{
		return static_cast<T const*>(reinterpret_cast<void*>(n));
	}

	std::intptr_t offset_;
	bool isNull_;
};

#endif // OffsetPtr_h_INCLUDED
