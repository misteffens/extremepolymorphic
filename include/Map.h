//
// Copyright (C) 2024 Dr. Michael Steffens
//
// SPDX-License-Identifier:     BSL-1.0
//

#ifndef Map_h_INCLUDED
#define Map_h_INCLUDED

#include "NonRelocatable/Map.h"
#include "Relocatable/OffsetPtr/Map.h"

template<
	typename Key,
	typename T,
	typename Compare = std::less<Key>,
	typename Alloc = std::pmr::polymorphic_allocator<std::pair<const Key, T>>>
using Map = Relocatable::OffsetPtr::Map<Key, T, Compare, Alloc>;

#endif // Map_h_INCLUDED
